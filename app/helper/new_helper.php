<?php
if ( ! function_exists('check_not_empty_group'))
{
	/**
	 * Add an element to an array if it doesn't exist.
	 *
	 * @param  array   $array
	 * @return array
	 */
	function check_not_empty_group($array)
	{
		foreach($array as $arr)
		{
           if (!empty($arr))
           {
           	 return true;
           }
		}
		return false;
	}
}

if ( ! function_exists('cdebug'))
{
	/**
	 * Add an element to an array if it doesn't exist.
	 *
	 * @param  array   $array
	 * @return array
	 */
	function cdebug($array)
	{
		try{
			echo "<pre>";
			print_r($array);
			echo "</pre>";
		}catch(Exception $e){
			echo "Parameter must an array.";
		}
	}
}