<?php
define('ACTIVE',1);
define('INACTIVE',0);
define('ENABLE_DISTRICT' ,1);
define('DISENABLE_DISTRICT' ,1);
define('ENGLISH',0);
define('VIETNAM',1);
define('THAILAN',2);
define('LANG_ENGLISH','en');
define('LANG_VIETNAM','vi');
define('LANG_THAILAN','th');
define('INQUIRY_ONLY_SMS',0);
define('INQUIRY_SMS_AND_VENDOR',1);
define('BANNER_HEADER',0);
define('BANNER_RIGHT',1);
define('SP_SEVICE_INFO','service');
define('SP_TERM_SERVICE','term of service');
define('SP_AD','advertisement');
define('SP_ABOUT_US','about us');
