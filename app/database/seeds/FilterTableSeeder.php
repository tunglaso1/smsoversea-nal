<?php
class FilterTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('facilities')->truncate();
		Filter::insert(array(
			array('criteria'=>'province-district','description'=>'multi select','created_at'=>'2014-04-04 04:04:04','updated_at'=>'2014-04-04 04:04:04'),
			array('criteria'=>'category search','description'=>'select box or multi select','created_at'=>'2014-04-04 04:04:04','updated_at'=>'2014-04-04 04:04:04'),
			array('criteria'=>'facility name','description'=>'like','created_at'=>'2014-04-04 04:04:04','updated_at'=>'2014-04-04 04:04:04'),
			array('criteria'=>'plan cost','description'=>'from ~ to','created_at'=>'2014-04-04 04:04:04','updated_at'=>'2014-04-04 04:04:04')
			));

		// Uncomment the below to run the seeder
	}

}