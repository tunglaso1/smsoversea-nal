<?php
class UsersTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('facilities')->truncate();

		$user = array(
			'username' => 'admin_minor',
			'email'    => 'nadia.hungn@gmail.com',
			'password' => Hash::make('123456')
		);
		User::create($user);

		// Uncomment the below to run the seeder
		// DB::table('facilities')->insert($facilities);
	}

}