<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullMapAccess extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::update("ALTER TABLE `map_access` CHANGE `address` `address` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `map_access` CHANGE `map_image` `map_image` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `map_access` CHANGE `direction_info` `direction_info` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
