<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',200);
            $table->string('description',500)->nullable();
            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// Drop categories tables
		Schema::drop('categories');
	}

}
