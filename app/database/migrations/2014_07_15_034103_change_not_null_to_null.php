<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNotNullToNull extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::update("ALTER TABLE `banners` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `banners` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `banners` CHANGE `image_url` `image_url` VARCHAR(400) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `categories` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `categories` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `companies` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `companies` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `districts` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `districts` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `facilities` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `facilities` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `galleries` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `galleries` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `galleries` CHANGE `image_url` `image_url` VARCHAR(400) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `inquiries` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `inquiries` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `language_contents` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `language_contents` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `map_access` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `map_access` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `map_access` CHANGE `map_image` `map_image` VARCHAR(400) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `plants` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `plants` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `provinces` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `provinces` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `search_filters` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `search_filters` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `static_contents` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `static_contents` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `system_settings` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `system_settings` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");

		DB::update("ALTER TABLE `users` CHANGE `create_user` `create_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `users` CHANGE `update_user` `update_user` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
