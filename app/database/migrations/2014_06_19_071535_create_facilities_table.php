<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFacilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('facilities', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name',200);
			$table->tinyInteger('status')->default(1);
			$table->dateTime('open_date');
			$table->string('homepage',200)->nullable();
			$table->string('facility_use_status',1000)->nullable();
			$table->string('sum_area',200)->nullable();
			$table->string('sum_room_area',200)->nullable();
			$table->string('room_number',1000)->nullable();
			$table->string('private_room_number',1000)->nullable();
			$table->string('room_area',1000)->nullable();
			$table->string('other_info',1000)->nullable();
			$table->string('facility_image',1000)->nullable();
			$table->string('age',1000)->nullable();
			$table->string('healthy_image',1000)->nullable();
			$table->string('avaiable_ilness',1000)->nullable();
			$table->string('not_avaiable_ilness',1000)->nullable();
			$table->string('medical_treatment',1000)->nullable();
			$table->string('tel',200)->nullable();
			$table->string('mobile',200)->nullable();
			$table->string('email',200)->nullable();
			$table->string('contact_persion',200)->nullable();
			$table->string('contact_remark',1000)->nullable();
			$table->string('skype',200)->nullable();
			$table->string('contact_avaiable_time',1000)->nullable();
			$table->tinyInteger('is_receive_inquiry')->default(0);
			$table->text('daily_timeable');
			$table->smallInteger('priority')->default(0);
			$table->string('create_user',200);
			$table->string('update_user',200);

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('facilities');
	}

}
