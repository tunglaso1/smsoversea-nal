<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('username', 200)->unique();
            $table->string('password', 200);
            $table->string('email',200)->unique();
            $table->string('description')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string("remember_token")->nullable();
            $table->string('create_user',200);
            $table->string('update_user',200);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
