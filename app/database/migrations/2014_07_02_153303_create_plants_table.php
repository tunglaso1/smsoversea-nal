<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('plants', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('facility_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();
            $table->string('plan_name',200);
            $table->double('plan_code',15,8);
            $table->string('plan_description',1000);
          
            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
            $table->foreign('facility_id')->references('id')->on('facilities');
            $table->foreign('category_id')->references('id')->on('categories');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('plants');
	}

}
