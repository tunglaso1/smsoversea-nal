<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInquiriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('inquiries', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->tinyInteger('inquiry_type')->default(1);
            $table->string('inquiry_email',200);
            $table->string('inquiry_mobile',200);
            $table->text('inquiry_content');
            $table->string('memo',200);
            $table->string('confirm_mail_status',200);

            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('inquiries');
	}

}
