<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMapAccessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('map_access', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('facility_id')->unsigned()->index();
            $table->integer('province_id')->unsigned()->index();
            $table->integer('district_id')->unsigned()->index();
            $table->string('address',1000);
            $table->string('map_image',400);
            $table->string('direction_info',1000);

            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
            
            $table->foreign('facility_id')->references('id')->on('facilities');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('district_id')->references('id')->on('districts');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('map_access');
	}

}
