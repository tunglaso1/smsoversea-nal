<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('companies', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('facility_id')->unsigned()->index();
            $table->string('name',200);
            $table->string('persion_in_charge',200);
            $table->dateTime('open_date');
            $table->string('address',1000);
            $table->string('contact_info',200);
            $table->string('homepage',200);

            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
            $table->foreign('facility_id')->references('id')->on('facilities');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('companies');
	}

}
