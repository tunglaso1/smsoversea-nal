<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvincesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('provinces', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',200);
            $table->tinyInteger('is_view_district')->default(0);
            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('provinces');
	}

}
