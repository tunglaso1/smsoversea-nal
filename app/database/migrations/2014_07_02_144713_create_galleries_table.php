<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('galleries', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('facility_id')->unsigned()->index();
            $table->string('gallery_name',200)->nullable();
            $table->string('image_url',400);
            $table->string('description',200)->nullable();
            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
            
            $table->foreign('facility_id')->references('id')->on('facilities');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('galleries');
	}

}
