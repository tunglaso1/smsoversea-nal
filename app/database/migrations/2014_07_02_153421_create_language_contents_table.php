<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguageContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('language_contents', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('modelable_id')->unsigned();
            $table->string('modelable_type',200);
            $table->string('attribute',200);
            $table->string('language_code',2);
            $table->text('content');
          
            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
            $table->unique(array('modelable_id','modelable_type','attribute','language_code'),'lang_model_attribute_code_index');
            //$table->primary(array('model_id','model_name','attribute','language_code'));
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('language_contents');
	}

}
