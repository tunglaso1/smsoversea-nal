<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistrictsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('districts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('province_id')->unsigned()->index();
            $table->string('name',200);
            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
            $table->foreign('province_id')->references('id')->on('provinces');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('districts');
	}

}
