<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilityCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('facilities_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('category_id')->unsigned()->index();
            $table->integer('facility_id')->unsigned()->index();
            $table->string('basic_service_avaiable',1000);
			$table->string('basic_service_notavaiable',1000);
            //$table->primary(array('category_id','facility_id'));
            $table->unique(array('category_id','facility_id'));
            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('facility_id')->references('id')->on('facilities');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('facilities_categories');
	}

}
