<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticContentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('static_contents', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',200);
            $table->text('content');
            $table->tinyInteger('status')->default(1);
          
            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('static_contents');
	}

}
