<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeNullCategories extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::update("ALTER TABLE `companies` CHANGE `name` `name` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `companies` CHANGE `persion_in_charge` `person_in_charge` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `companies` CHANGE `open_date` `open_date` DATETIME NULL");
		DB::update("ALTER TABLE `companies` CHANGE `address` `address` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `companies` CHANGE `contact_info` `contact_info` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
		DB::update("ALTER TABLE `companies` CHANGE `homepage` `homepage` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::update("ALTER TABLE `companies` CHANGE `person_in_charge` `persion_in_charge` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL");
	}

}
