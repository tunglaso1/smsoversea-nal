<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('system_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('setting_code',3);
            $table->string('setting_name',200);
            $table->string('description',200);
            $table->string('value',200);
          
            $table->string('create_user',200);
            $table->string('update_user',200);
            $table->timestamps();
            $table->primary('setting_code');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('system_settings');
	}

}
