<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeToSearchFilter extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('search_filters', function($table)
		{
		    $table->tinyInteger('type')->after('status')->default(0);
		    $table->string('description',1000)->after('type')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('search_filters', function($table)
		{
		    $table->dropColumn('type','description');
		});
	}

}
