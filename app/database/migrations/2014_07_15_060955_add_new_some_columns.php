<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewSomeColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{   
		Schema::rename('plants', 'plans');
		Schema::table('plans', function($table)
		{
		    $table->string('plan_unit',200)->after('plan_description')->nullable();
		});
		Schema::table('facilities', function($table)
		{
		    $table->string('sharing_facility',1000)->nullable();
		    $table->string('room_max_patien',1000)->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::rename('plans', 'plants');
		Schema::table('plants', function($table)
		{
		    $table->dropColumn('plan_unit');
		});
		Schema::table('facilities', function($table)
		{
		    $table->dropColumn('sharing_facility','room_max_patien');
		});
	}

}
