<?php 
class CImage
{
  const PATH_UPLOAD_IMAGE = 'uploads/';

  private $sub_path;

  public function __construct($sub)
  {
     $this->sub_path   = $sub;
  }

  private function getSubPath()
  {
    return $this->sub_path;
  }

  private function checkExistFolder($id)
  {
    if (is_dir(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath().'/'.$id)){
        return true;
    }else{
        return false;
    }
  }

	public function test($ob){
       var_dump($ob);die;
	}
  /** 
  */
	public function upload($ob,$id){
    $test = $this->checkExistFolder($id);
    if ($test)
    {
      $this->delete(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath().'/'.$id);
    }
    $this->create($id);
    $filename           = $ob->getClientOriginalName();
    $name_file_saved    = time().'_'.$filename;
    $img = Image::make($ob);
    // resize image instance
    $img->resize(100, 100);
    $img->save(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath().'/'.$id.'/thumb/'.$name_file_saved);
    //
    $ob->move(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath().'/'.$id.'/origin', $name_file_saved);

    return $this->getSubPath().'/'.$id.'/origin/'.$name_file_saved;
	}

  private function create($id){
    if (!is_dir(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath()))
    {
       mkdir(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath(),0777, true);
    }
    mkdir(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath().'/'.$id,0777, true);
    mkdir(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath().'/'.$id.'/thumb',0777, true);
    //mkdir(static::PATH_UPLOAD_IMAGE.getSubPath().'/'.$id.'/medium',0777, true);
    mkdir(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath().'/'.$id.'/origin',0777, true);
  }

  private function delete($path){
    $di = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
    $ri = new RecursiveIteratorIterator($di, RecursiveIteratorIterator::CHILD_FIRST);
    foreach ( $ri as $files ) {
      $files->isDir() ?  rmdir($files) : unlink($files);
    }
    rmdir($path);
  }

  public function exeDelete($id)
  {
      $this->delete(public_path().'/'.static::PATH_UPLOAD_IMAGE.$this->getSubPath().'/'.$id);
      return true;
  }

}