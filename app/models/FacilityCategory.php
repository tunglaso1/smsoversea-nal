<?php
class FacilityCategory extends Eloquent {

	protected $table = 'facilities_categories';

	public $timestamps = false;

	protected $guarded = array();

	public static $rules = array();

	public function category(){
		return $this->belongsTo('Category');
	} 
 
}
