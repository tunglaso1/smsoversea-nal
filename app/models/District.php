<?php
class District extends Celoquent{
	protected $guarded = array();

	protected $arr_attr_trans = array('name');

	public $arr_relations = array();

	public static $rules = array(
	);

	public function languagecontents(){
		return $this->morphMany('LanguageContent', 'modelable');
	} 
}
