<?php
class Category extends Eloquent {
    protected $guarded = array();

    public static function rules ($id=0) {
        return [
            'name' => 'required|unique:categories,name' . ($id ? ",$id" : '')
        ];
    }

    public static function validate($data,$id=0)
    {
        return Validator::make($data,Category::rules($id));
    }

}