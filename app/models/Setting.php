<?php
class Setting extends Eloquent {
    protected $guarded = array();

    protected $table = 'system_settings';
    protected $primaryKey = 'setting_code';

    public static function rules ($setting_code=0) {
        return [
            'setting_name' => 'required|unique:system_settings,setting_name' . ($setting_code ? ",$setting_code,setting_code" : ''),
            'value' => 'required'
        ];
    }

    public static function validate($data,$setting_code='')
    {
        return Validator::make($data,static::rules($setting_code));
    }

}