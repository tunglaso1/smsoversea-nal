<?php
class StaticPage extends Eloquent {
    protected $guarded = array();

    protected $table = 'static_contents';

    public static function rules ($id=0) {
        return [
            'content' =>'required',
            'name' => 'required|unique:static_contents,name' . ($id ? ",$id" : '')
        ];
    }

    public static function validate($data,$id=0)
    {
        return Validator::make($data,static::rules($id));
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function($static_page)
        {
            if (!is_null(Auth::user())){
                $static_page->create_user = Auth::user()->username;
                $static_page->update_user = Auth::user()->username;
            }
        });

        static::updating(function($static_page)
        {
            if (!is_null(Auth::user())){
                $static_page->update_user = Auth::user()->username;
            }
        });
    }


}