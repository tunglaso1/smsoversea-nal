<?php
use LaravelPrototype\Interfaces\BookInterface;
class Book extends Eloquent implements BookInterface {
	protected $guarded = array();

	public static $rules = array(
		'name' => 'required',
		'category' => 'required',
		'des' => 'required'
	);

	public function test()
	{
		return array("name"=>"hung test","age"=>"19","email"=>"email");
	}

}
