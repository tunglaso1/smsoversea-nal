<?php

class Inquiry extends Eloquent {
	protected $guarded = array();

	public static function rules($id=null)
	{
		return array(
        'inquiry_type'	=>'required|numeric|between:0,1',
        'inquiry_email'		=>'required|email|unique:inquiries,inquiry_email'. ($id ? ",$id" : '')
        );
	} 

    public static function validate($data,$id=null)
    {
        return Validator::make($data,static::rules($id));
    }

    /**
    * Save user created and user updated automatic
    */
    public static function boot()
    {
        parent::boot();

        static::updating(function($inquiry)
        {
            if (!is_null(Auth::user())){
                $inquiry->update_user = Auth::user()->username;
            }
        });
    }
}
