<?php

class Banner extends Eloquent {
    const PRE_IMAGE = '';
	protected $guarded = array();

	public static $rules = array(
        'banner_position'   =>'required|numeric|between:0,1',
        'priority'      =>'required|numeric',
        'start' =>'date_format:"Y-m-d H:i:s"',
        'end' =>'date_format:"Y-m-d H:i:s"'
    );

    public static function validate($data)
    {
        return Validator::make($data,static::$rules);
    }

    public function getThumb()
    {
        $thum = str_replace('origin','thumb',$this->image_url);
        return static::PRE_IMAGE.'uploads/'.$thum;
    }

    public function getOrigin()
    {
        return static::PRE_IMAGE.'uploads/'.$this->image_url;
    }
}
