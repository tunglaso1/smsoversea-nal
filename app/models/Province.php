<?php
class Province extends Celoquent{
	public static $lang;

	protected $guarded = array();

	protected $arr_attr_trans = array('name');

	public $arr_relations = array();

	public static $rules = array(
	);

	public static function setLang($la)
	{
		static::$lang = $la;
	}

	public function languagecontents(){
		$sql = $this->morphMany('LanguageContent', 'modelable');
		if (static::$lang != null)
		{
			$sql = $sql->where('language_code','=',static::$lang);
		}
		$sql = $sql->orderBy('attribute')->orderBy('content');
		return $sql;
	} 


	public function getName($attr)
	{
		foreach ($this->languagecontents as $element)
		{
             if($element->attribute == $attr)
             {
             	return $element->content;
             }
		}
		return "";
	}
    
	// public function delete()
	// {
	//     $this->device()->delete();
	// 	$this->realPass()->delete();
	//     // delete the user
	// 	return parent::delete();
	// }
}
