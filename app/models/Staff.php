<?php
class Staff extends Celoquent { 
	protected $guarded = array();

	public static $rules = array(
	);
	
	public function photos(){
		return $this->morphMany('Photo', 'imageable');
	} 
}