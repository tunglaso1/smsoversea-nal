<?php

class Facility extends Eloquent {
	protected $guarded = array();

	public static $rules = array(
		'homepage'   =>'url',
        'age'      =>'numeric',
        'open_date' =>'date_format:"Y-m-d H:i:s"'
	);

	public static $ruleCompany = array(
		'homepage'   =>'url',
        'open_date' =>'date_format:"Y-m-d H:i:s"'
	);
	
	public static $arr_status = array(
    	 INACTIVE => 'Inactive',
    	 ACTIVE   => 'Active'
    );

    public static function validate($data)
    {
        return Validator::make($data,static::$rules);
    }

    public static function validateCompany($data)
    {
        return Validator::make($data,static::$ruleCompany);
    }

	public function categories()
	{
		return $this->belongsToMany('Category','facilities_categories','facility_id','category_id');
	}

	public function company()
	{
		return $this->hasOne('Company','facility_id');
	}

	public function getArrCategories()
	{
		$results = array();
		foreach ($this->categories as $category){
			$results[$category->id] = $category->name;
		}
		return $results;
	}

	/**
	 * Deletes a blog post and all
	 * the associated comments.
	 *
	 * @return bool
	 */
	public function delete()
	{
		// Delete the categories
		$this->categories()->delete();

		// Delete the facility
		return parent::delete();
	}

	public function saveAllRelation($input)
	{	
		$this->update_user = Auth::user()->username;
        $this->update(array_except($input,array('company')));
        if (is_null($this->company)){
        	// create
        	$company = new Company($input['company']);
        	$this->company()->save($company);

        }else{
        	//update
        	$this->company()->update($input['company']);
        }
        return $this;
	}

	public function getThumb()
    {
        $thum = str_replace('origin','thumb',$this->facility_image);
        return 'uploads/'.$thum;
    }
}
