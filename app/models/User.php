<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
	protected $guarded = array(); 

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

    /**
     * Validation rules for user
     *
     * @var array
     */
    public static $rules = array(
        'username'	=>'required|alpha_dash|min:4|unique:users',
        'email'		=>'required|email|unique:users',
        'password'	=>'required|alpha_num|min:6|confirmed'
    );

    public static $arr_status = array(
    	 INACTIVE => 'Inactive',
    	 ACTIVE   => 'Active'
    	);

    public static function validate($data)
    {
    	return Validator::make($data,static::$rules);
    }

    /**
    * Save user created and user updated automatic
    */
    public static function boot()
    {
        parent::boot();

        static::creating(function($user)
        {
        	if (!is_null(Auth::user())){
        		$user->create_user = Auth::user()->username;
            	$user->update_user = Auth::user()->username;
        	}
        });

        static::updating(function($user)
        {
            if (!is_null(Auth::user())){
            	$user->update_user = Auth::user()->username;
        	}
        });
    }

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

}
