<?php
class Celoquent extends Eloquent {
	protected $arr_attr_trans = array();

	protected $arr_langs = array(LANG_ENGLISH,LANG_THAILAN);

	protected $has_file = false;

	public $arr_relations = array();

	public function csave(array $inputs = array())
    {
    	$major_class = $this->create($inputs['major']);
    	//$class_name = get_class($this);
    	//echo $class_name;die;
    	foreach($this->arr_langs as $lang)
    	{
    		foreach($this->arr_attr_trans as $attr)
    		{
    			$obLang = new LanguageContent();

    			$obLang->language_code = $lang;

    			$obLang->attribute = $attr;

    			$obLang->content = $inputs[$lang][$attr];

    			$obLang->create_user = Auth::user()->username;

    			$obLang->update_user = Auth::user()->username;

    			$major_class->languagecontents()->save($obLang);

    		}
    	}
    	return $major_class;
    }

    public function cupdate(array $inputs = array(),$id)
    {
    	$major_class = $this->find($id);
    	$major_class->update($inputs['major']);
    	foreach($major_class->languagecontents as $obLang)
    	{
    		$obLang->content = $inputs[$obLang->language_code][$obLang->attribute];
    		$obLang->update_user = Auth::user()->username;
    		$obLang->save();
    	}
    	return $major_class;
    }

    public function cdelete($id)
    {
    	$major_class = $this->find($id);
    	$major_class->languagecontents()->delete();
    	$major_class->delete();
    }
	
}