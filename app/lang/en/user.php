<?php

return array(

	/*
	|--------------------------------------------------------------------------
	|
	*/

	"list_user" => "List Users",

	"add_new_user" 		=> "Add New User",

	"username" 	=> "Username",

	"email" 	=> "Email",

	"des" 	=> "Description",

	"status"  	=> "Status",

	"action"   	=> "Action",

	"delete"=> "Delete",

	"edit"   => "Edit",

	"add"  => "Add",

	"update"  => "Update",

	"create" => "Create",

	"cancel"  => "Cancel",

	"switch" => "Switch",

	"reset_pass" => "Reset Pass",

	"password"   => "Password",

	"password_confirm" => "Confirm Pass",

	"edit_user" => "Edit User",

	"active" => "Active",

	"inactive" => "Inactive",

	"all_user" => "All User",

);
