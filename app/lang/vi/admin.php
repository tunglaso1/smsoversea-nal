<?php

return array(

	/*
	|--------------------------------------------------------------------------
	|
	*/

	"dashboard" => "Dashboard",

	"user" 		=> "Users",

	"login" 	=> "Login",

	"logout" 	=> "Logout",

	"category" 	=> "Categories",

	"setting"  	=> "Setting",

	"master"   	=> "Master",

	"title_logo"=> "SMS Overse",

	"english"   => "English",

	"vietname"  => "Vietnamese",

	"thailand"  => "Thailand",

);
