<?php

return array(

	/*
	|--------------------------------------------------------------------------
	|
	*/

	"list_user" => "Danh sách tài khoản",

	"add_new_user" 		=> "Tạo mới",

	"username" 	=> "Tên đăng nhập",

	"email" 	=> "Email",

	"des" 	=> "Mô tả",

	"status"  	=> "Trạng thái",

	"action"   	=> "Chức năng",

	"delete"=> "Xoá",

	"edit"   => "Sửa",

	"add"  => "Tạo Mới",

	"update"  => "Cập nhật",

	"create" => "Tạo mới",

	"cancel"  => "Huỷ",

	"switch" => "Chuyển nhanh",

	"reset_pass" => "Khôi phục pass",

	"password"   => "Mật khẩu",

	"password_confirm" => "Xác nhận lại",

	"edit_user" => "Chỉnh sửa",

	"active" => "Đang sử dụng",

	"inactive" => "Không sử dụng",

	"all_user" => "Tất cả",

);
