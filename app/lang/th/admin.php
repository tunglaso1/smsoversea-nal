<?php

return array(

	/*
	|--------------------------------------------------------------------------
	|
	*/

	"dashboard" => "Bang Dieu khien",

	"user" 		=> "Nguoi Dung",

	"login" 	=> "Login",

	"logout" 	=> "Logout",

	"category" 	=> "Danh Muc",

	"setting"  	=> "Cai Dat",

	"master"   	=> "Master",

	"title_logo"=> "SMS Overse",

	"english"   => "Tieng Anh",

	"vietname"  => "Tieng Viet",

	"thailand"  => "Tieng Thai",

);
