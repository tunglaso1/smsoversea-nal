<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
// Set Locale
$locale = Request::segment(1);
if (in_array($locale, Config::get('app.languages'))) {
    App::setLocale($locale);
} else {
    $locale = null;
}
Route::get('ck',function(){
    return View::make('ck');
});
Route::post('post_data',function(){
    $content1 = Input::get('editor11');
    return View::make('post_ck',compact('content1'));
    //cdebug(Input::all());
});
// Router
Route::group(array('prefix' => $locale), function() {
	Route::get('/', function()
	{
		//die(var_dump(App::make('foo')));
		$loca = Request::segment(1);
        if ($loca == '') {
            $default_loca = App::getLocale();
            //return Redirect::to("/{$default_loca}");
        }
    	return View::make('hello');
	});

	Route::get('test','HomeController@test');

	//Change Language
	Route::post('change_locale', array('as' => 'change_locale', function() {
	    //Set locale
	    $locale = Input::get('set_locale');
	    App::setLocale($locale);
	    //Get last route from session
	    $route = Session::get('lastest_route');
	    //Convert route to array
	    $array_route = explode("/", $route);
	    //Change first segment of route to "es", "en" or whatever
	    if (strlen($array_route[0]) > 2)
	    {
          array_unshift($array_route, $locale);
	    }
	    else
	    {
	    	$array_route[0] = $locale;
	    }
	    //Convert array to string
	    $redirect_route = implode("/", $array_route);
	    //Redirect to new route
	    return Redirect::to($redirect_route);
	}));

	// login function
	Route::group(array('prefix' => 'admin'), function()
	{
		Route::get('/', array('uses' => 'LoginManagementController@getLogin','as'=>'users.login'));
	
		Route::post('login', array('uses' => 'LoginManagementController@doLogin','as'=>'users.doLogin'));
	});

	Route::group(array('prefix' => 'admin','before' => 'auth'), function()
	{
		Route::get('logout', array('uses' => 'LoginManagementController@getLogout','as'=>'users.logout'));

		Route::get('dashboard', array('uses' => 'Admin\DashboardController@index','as'=>'dashboard.index'));
	
		Route::get('changePass', array('uses' => 'LoginManagementController@changePass','as'=>'users.changePass'));

		Route::post('changePass', array('uses' => 'LoginManagementController@doChange','as'=>'users.doChange'));

		Route::get('users/{users}/switch', array('uses' => 'Admin\UsersController@switchStatus','as'=>'admin.users.switch'));

		Route::get('users/{users}/resetPass', array('uses' => 'Admin\UsersController@resetPass','as'=>'admin.users.resetPass'));
	
		Route::resource('users', 'Admin\UsersController',array('names' => array('index'  => 'admin.users.index'
																	 ,'create' =>'admin.users.create'
																	 ,'store'  =>'admin.users.store'
																	 ,'show'   =>'admin.users.show'
																	 ,'edit'   =>'admin.users.edit'
																	 ,'update' =>'admin.users.update'
																	 ,'destroy'=>'admin.users.destroy'
																)));

        Route::resource('categories', 'Admin\CategoriesController',array('names' => array('index'  => 'admin.categories.index'
        															,'create' =>'admin.categories.create'
        															,'store'  =>'admin.categories.store'
        															,'show'   =>'admin.categories.show'
        															,'edit'   =>'admin.categories.edit'
        															,'update' =>'admin.categories.update'
        															,'destroy'=>'admin.categories.destroy'
       															)));

        Route::resource('provinces', 'Admin\ProvincesController',array('names' => array('index'  => 'admin.provinces.index'
        															,'create' =>'admin.provinces.create'
        															,'store'  =>'admin.provinces.store'
        															,'show'   =>'admin.provinces.show'
        															,'edit'   =>'admin.provinces.edit'
        															,'update' =>'admin.provinces.update'
        															,'destroy'=>'admin.provinces.destroy'
       															)));

        Route::resource('provinces.districts', 'Admin\DistrictsController',array('names' => array('index'  => 'admin.provinces.districts.index'
        															,'create' =>'admin.provinces.districts.create'
        															,'store'  =>'admin.provinces.districts.store'
        															,'show'   =>'admin.provinces.districts.show'
        															,'edit'   =>'admin.provinces.districts.edit'
        															,'update' =>'admin.provinces.districts.update'
        															,'destroy'=>'admin.provinces.districts.destroy'
       															)));

        Route::resource('facilities', 'Admin\FacilitiesController',array('names' => array('index'  => 'admin.facilities.index'
        															,'create' =>'admin.facilities.create'
        															,'store'  =>'admin.facilities.store'
        															,'show'   =>'admin.facilities.show'
        															,'edit'   =>'admin.facilities.edit'
        															,'update' =>'admin.facilities.update'
        															,'destroy'=>'admin.facilities.destroy'
       															)));

        Route::get('facilities/{facilities}/category', array('uses' => 'Admin\FacilitiesController@getCategory','as'=>'admin.facilities.getCategory'));

        Route::get('facilities/{facilities}/service', array('uses' => 'Admin\FacilitiesController@getService','as'=>'admin.facilities.getService'));

        Route::get('facilities/{facilities}/access', array('uses' => 'Admin\FacilitiesController@getAccess','as'=>'admin.facilities.getAccess'));

        Route::get('facilities/{facilities}/timeable', array('uses' => 'Admin\FacilitiesController@getTimeable','as'=>'admin.facilities.getTimeable'));

        Route::get('facilities/{facilities}/gallery', array('uses' => 'Admin\FacilitiesController@getGallery','as'=>'admin.facilities.getGallery'));

        Route::get('facilities/{facilities}/contact', array('uses' => 'Admin\FacilitiesController@getContact','as'=>'admin.facilities.getContact'));

        Route::get('facilities/{facilities}/other', array('uses' => 'Admin\FacilitiesController@getOther','as'=>'admin.facilities.getOther'));

        Route::patch('facilities/{facilities}/categories', array('uses' => 'Admin\FacilitiesController@updateCategories','as'=>'admin.facilities.updateCategories'));

        Route::patch('facilities/{facilities}/service', array('uses' => 'Admin\FacilitiesController@updateService','as'=>'admin.facilities.updateService'));

        Route::patch('facilities/{facilities}/timeable', array('uses' => 'Admin\FacilitiesController@updateTimeable','as'=>'admin.facilities.updateTimeable'));

        Route::patch('facilities/{facilities}/gallery', array('uses' => 'Admin\FacilitiesController@updateGallery','as'=>'admin.facilities.updateGallery'));

        Route::patch('facilities/{facilities}/access', array('uses' => 'Admin\FacilitiesController@updateAccess','as'=>'admin.facilities.updateAccess'));

        Route::patch('facilities/{facilities}/contact', array('uses' => 'Admin\FacilitiesController@updateContact','as'=>'admin.facilities.updateContact'));

        Route::patch('facilities/{facilities}/other', array('uses' => 'Admin\FacilitiesController@updateOther','as'=>'admin.facilities.updateOther'));

        Route::post('facilities/{facilities}/switch', array('uses' => 'Admin\FacilitiesController@switchStatus','as'=>'admin.facilities.switchStatus'));

        Route::post('facilities/{facilities}/cate/{categories}', array('uses' => 'Admin\FacilitiesController@addNewPlan','as'=>'admin.facilities.addNewPlan'));

        Route::delete('plans/{facilities}/cate/{categories}/plan/{plans}', array('uses' => 'Admin\PlansController@deletePlan','as'=>'admin.plans.deletePlan'));

        Route::patch('plans/{facilities}/cate/{categories}/plan/{plans}', array('uses' => 'Admin\PlansController@updatePlan','as'=>'admin.plans.updatePlan'));

        Route::get('plans/{facilities}/cate/{categories}/plan/{plans}', array('uses' => 'Admin\PlansController@editPlan','as'=>'admin.plans.editPlan'));

        Route::resource('inquiries', 'Admin\InquiriesController',array('names' => array('index'  => 'admin.inquiries.index'
        															,'create' =>'admin.inquiries.create'
        															,'store'  =>'admin.inquiries.store'
        															,'show'   =>'admin.inquiries.show'
        															,'edit'   =>'admin.inquiries.edit'
        															,'update' =>'admin.inquiries.update'
        															,'destroy'=>'admin.inquiries.destroy'
       															)));


        Route::resource('banners', 'Admin\BannersController',array('names' => array('index'  => 'admin.banners.index'
        															,'create' =>'admin.banners.create'
        															,'store'  =>'admin.banners.store'
        															,'show'   =>'admin.banners.show'
        															,'edit'   =>'admin.banners.edit'
        															,'update' =>'admin.banners.update'
        															,'destroy'=>'admin.banners.destroy'
       															)));
        
        Route::resource('settings', 'Admin\SettingsController',array('only' => array('index','update','edit'
        ), "names" => array('index'  => 'admin.settings.index' ,'edit' =>'admin.settings.edit' ,'update'  =>'admin.settings.update')));


        Route::resource('statics', 'Admin\StaticPageController',array('names' => array('index'  => 'admin.pages.index'
                                                                    ,'create' =>'admin.pages.create'
                                                                    ,'store'  =>'admin.pages.store'
                                                                    ,'show'   =>'admin.pages.show'
                                                                    ,'edit'   =>'admin.pages.edit'
                                                                    ,'update' =>'admin.pages.update'
                                                                    ,'destroy'=>'admin.pages.destroy'
                                                                )));

        Route::resource('filters', 'Admin\FiltersController',array('only'=>array('index','edit','update')
                                                                  ,'names' => array('index'  => 'admin.filters.index'
                                                                                   ,'edit'   =>'admin.filters.edit'
                                                                                   ,'update' =>'admin.filters.update'
                                                                 )));
	});

});

// Route::get('/', function()
// {
// 	// if (Cache::has('book'))
// 	// {
// 	// 	Cache::forget('book');
// 	// }
// 	// else
// 	// {
// 	// 	Cache::put('book',Book::all(),30);
// 	// }
// 	// return Cache::get('book');
// 	return View::make('test');
// });
