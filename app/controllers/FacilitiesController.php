<?php

class FacilitiesController extends BaseController {

	/**
	 * Facility Repository
	 *
	 * @var Facility
	 */
	protected $facility;
    protected $filter;

	public function __construct(Facility $facility, Filter $filter)
	{
		$this->facility = $facility;
        $this->filter = $filter;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$facilities = $this->facility->all();
        $filters = $this->_getActiveFilters();

		return View::make('facilities.index', compact('facilities', 'filters'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('facilities.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		$validation = Validator::make($input, Facility::$rules);

		if ($validation->passes())
		{
			$this->facility->create($input);

			return Redirect::route('facilities.index');
		}

		return Redirect::route('facilities.create')
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$facility = $this->facility->findOrFail($id);

		return View::make('facilities.show', compact('facility'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$facility = $this->facility->find($id);

		if (is_null($facility))
		{
			return Redirect::route('facilities.index');
		}

		return View::make('facilities.edit', compact('facility'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Facility::$rules);

		if ($validation->passes())
		{
			$facility = $this->facility->find($id);
			$facility->update($input);

			return Redirect::route('facilities.show', $id);
		}

		return Redirect::route('facilities.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', 'There were validation errors.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->facility->find($id)->delete();

		return Redirect::route('facilities.index');
	}

    public function postSearch(){
        $filters = $this->_getActiveFilters();
        $input = Input::all();

        $facilities = $this->facility;

        if ($input['province'])
            $facilities = $facilities->where('province', $input['province']);

        if ($input['name'])
            $facilities = $facilities->where('name', 'like', '%' . $input['name'] . '%');

        foreach($filters as $filter){
            if ($input[$filter->criteria]){
                $facilities = $facilities->where($filter->criteria, $filter->operator ,$input[$filter->criteria]);
            }
        }

        $facilities = $facilities->get();

        return View::make('facilities.index', compact('facilities', 'input', 'filters'));
    }

    private function _getActiveFilters(){
        $filters = $this->filter->where('isApply', '1')->get();
        return $filters;
    }

}
