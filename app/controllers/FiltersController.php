<?php

class FiltersController extends BaseController {

	protected $filter;

	public function __construct(Filter $filter)
	{
		$this->filter = $filter;
	}

	public function getIndex()
	{
        $filters = $this->filter->all();
        return View::make('filters.index', compact("filters"));
	}

    public function getCreate(){
        return View::make('filters.create');
    }

    public function postStore(){
        $input = Input::all();
        $validation = Validator::make($input, Filter::$rules);

        if ($validation->passes())
        {
            $this->filter->create($input);

            return Redirect::to('filters/index');
        }

        return Redirect::to('filters/create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    public function getEdit($id)
    {
        $filter = $this->filter->find($id);

        if (is_null($filter))
        {
            return Redirect::to('filters/index');
        }

        return View::make('filters.edit', compact('filter'));
    }

    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Filter::$rules);

        if ($validation->passes())
        {
            $filter = $this->filter->find($id);
            $filter->update($input);

            return Redirect::to('filters/index');
        }

        return Redirect::to('filters/edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    public function postChangeStatus($id){
        $filter = $this->filter->find($id);
        $filter->isApply = $filter->isApply == 1 ? 0 : 1;
        $filter->save();
        return Redirect::to('filters/index');
    }

}
