<?php

class LoginManagementController extends BaseController {

	/**
	 * Book Repository
	 *
	 * @var LoginManagement
	 */
	//protected $layout = "layouts.scaffold";

	public function __construct()
    {
        // Perform CSRF check on all post/put/patch/delete requests
        $this->beforeFilter('csrf', array('on' => array('post')));
    }

	function getLogin()
	{
		if (Auth::check())
		{
            return Redirect::route('dashboard.index')->with('flash_notice', 'You are already logged in!');
		}
		
		return View::make('layouts.login');
	}

	function doLogin()
	{
		$user = array( 'password' => Input::get('password'),
					   'status'   => 1
		        );
		if (filter_var(Input::get('email'), FILTER_VALIDATE_EMAIL))	{
			$user = array_add($user, 'email', Input::get('email'));
		}
		else{
			$user = array_add($user, 'username', Input::get('email'));
		}	
		//	
		if (Auth::attempt($user)) 
		{
			return Redirect::route('dashboard.index')->with('flash_notice', 'You are successfully logged in.');
	    }
		// authentication failure! lets go back to the login page
		return Redirect::route('users.login')
				         ->with('flash_error', 'Your username/password combination was incorrect.')
				         ->withInput();

	}

	function changePass()
	{
        return View::make('login.change_pass');
	}

	function doChange()
	{
      if (!(Input::has('old_password'))) 
		{ 
			return Redirect::route('users.changePass')->with('flash_error', 'Your current password can not empty .'); 
		}
		
		if (Hash::check(Input::get('old_password'), Auth::user()->password)) {
			$rules  = array('password' => 'required|min:6|confirmed');
			$inputs = Input::all();
			$validation = Validator::make($inputs, $rules);

			if ($validation->passes())
			{
				$user = Auth::user();
				$user->password = Hash::make(Input::get('password'));
				$user->save();
			
				return Redirect::route('dashboard.index')
					->with('message', 'Your password has changed successfully');
			}

			return Redirect::route('users.changePass')
				->withInput()
				->withErrors($validation);
		}
		else {
		    return Redirect::route('users.changePass')->with('flash_error', 'Your current password is incorrect .'); 
		}
	}

	function getLogout()
	{
		Auth::logout();

		return Redirect::route('users.login')->with('flash_error', 'You are successfully logged out.');
	}

	

}
