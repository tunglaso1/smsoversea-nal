<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class FiltersController extends \BaseController {

	protected $filter;

	public function __construct(\Filter $filter)
	{
		$this->filter = $filter;
	}

	public function index()
	{
        $filters = $this->filter->all();
        return View::make('admin.filters.index', compact("filters"));
	}

    public function edit($id)
    {
        $filter = $this->filter->find($id);

        if (is_null($filter))
        {
            return Redirect::route('admin.filters.index')->with('message', 'Filter not found');
        }
        return View::make('admin.filters.edit', compact("filter"));
    }

    public function update($id)
    {
       $filter = $this->filter->find($id);
       $filter->status = Input::has('status') ?: INACTIVE;
       $filter->type = Input::has('type') ?: INACTIVE;
       $filter->save();
       return Redirect::route('admin.filters.index')->with('message', 'update success.');
    }

}
