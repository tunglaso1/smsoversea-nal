<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class DashboardController extends \BaseController {

    public function index()
    {
        return View::make('admin.dashboard.index');
    }

    
} 