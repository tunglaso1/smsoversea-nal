<?php
/**
 * Created by PhpStorm.
 * User: ducnn
 * Date: 2014-07-04
 * Time: 13:17
 */
namespace Admin;
use View;
use Input;
use Validator;
use Hash;
use Redirect;

class StaticPageController extends \BaseController {

    /**
     * Post Repository
     *
     * @var Post
     */
    protected $page;

    public function __construct(\StaticPage $page)
    {
        $this->beforeFilter('csrf', array('on' => array('post')));
        $this->page = $page;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pages = $this->page->all();
        return View::make('admin.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = \StaticPage::validate(Input::all());

        if ($validation->fails()){
            return Redirect::route('admin.pages.create')
                ->withInput()
                ->withErrors($validation);
        }else{
            $p  = Input::only('name','content');
            $this->page->create($p);
            return Redirect::route('admin.pages.index')->with('message', 'success.');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $page = $this->page->findOrFail($id);

        return View::make('admin.pages.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $page = $this->page->find($id);

        if (is_null($page))
        {
            return Redirect::route('admin.pages.index')->with('message', 'Page not found');
        }

        return View::make('admin.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validation = \StaticPage::validate(Input::all(),$id);

        if ($validation->fails()){
            return Redirect::route('admin.pages.edit', $id)
                ->withInput()
                ->withErrors($validation);
        }else{
            $input = Input::only('name','status','content');
            $page = $this->page->find($id);
            $page->update($input);
            return Redirect::route('admin.pages.show', $id)->with('message', 'update success.');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->page->find($id)->delete();

        return Redirect::route('admin.pages.index')->with('message', 'Delete success.');
    }

}