<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class DistrictsController extends \BaseController {

	protected $district;

    public function __construct(\District $district)
    {
        $this->beforeFilter('csrf', array('on' => array('post')));
        $this->district = $district;
    }

    public function index($id_p=0)
    {
    	$districts = $this->district->with('languagecontents')->where('province_id','=',$id_p)->get();
        return View::make('admin.districts.index',compact('districts','id_p'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create($id_p)
    {
        return View::make('admin.districts.create',compact('id_p'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store($id_p)
    {
        $arr_check = Array(
                        Input::get(LANG_ENGLISH.'.name')
                       //,Input::get(LANG_VIETNAM.'.name')
                       ,Input::get(LANG_THAILAN.'.name') 
                     );
        if (!check_not_empty_group($arr_check)){
            return Redirect::route('admin.districts.create')
                ->withInput()
                ->withErrors(array('name can not empty'));
        }else{
            $inputs = Input::all();
            $p  = $this->district->csave($inputs);
            return Redirect::route('admin.provinces.districts.index',array($p->province_id))->with('message', 'success.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id_p,$id)
    {
        $district = $this->district->findOrFail($id);

        return View::make('admin.districts.show', compact('district'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id_p,$id)
    {
        $district = $this->district->find($id);

        if (is_null($district))
        {
            return Redirect::route('admin.districts.index',array($id_p))->with('message', 'district not found');
        }

        return View::make('admin.districts.edit', compact('district','id_p'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id_p,$id)
    {
        $arr_check = Array(
                        Input::get(LANG_ENGLISH.'.name')
                       //,Input::get(LANG_VIETNAM.'.name')
                       ,Input::get(LANG_THAILAN.'.name') 
                     );
        if (!check_not_empty_group($arr_check)){
            return Redirect::route('admin.districts.edit',$id)
                ->withInput()
                ->withErrors(array('name can not empty'));
        }else{
            $inputs = Input::all();
            $p  = $this->district->cupdate($inputs,$id);
            return Redirect::route('admin.provinces.districts.index',array($id_p))->with('message', 'update success.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id_p,$id)
    {
        $this->district->cdelete($id);

        return Redirect::route('admin.provinces.districts.index',array($id_p))->with('message', 'Delete success.');
    }

    
} 