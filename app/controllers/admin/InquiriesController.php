<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class InquiriesController extends \BaseController {

    /**
     * Post Repository
     *
     * @var Post
     */
    protected $inquiry;

    public function __construct(\Inquiry $inquiry)
    {
        $this->beforeFilter('csrf', array('on' => array('post')));
        $this->inquiry = $inquiry;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $inquiries = $this->inquiry->paginate(100);

        return View::make('admin.inquiries.index', compact('inquiries'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.inquiries.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = \Inquiry::validate(Input::except("_token"));
        //dd($validation->messages()->all());die;

        if ($validation->fails()){
            return Redirect::route('admin.inquiries.create')
                ->withInput()
                ->withErrors($validation->messages()->all());
        }else{
            
            $this->inquiry->create(Input::except("_token"));
            return Redirect::route('admin.inquiries.index')->with('message', 'success.');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $inquiry = $this->inquiry->findOrFail($id);

        return View::make('admin.inquiries.show', compact('inquiry'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $inquiry = $this->inquiry->find($id);

        if (is_null($inquiry))
        {
            return Redirect::route('admin.inquiries.index')->with('message', 'Inquiry not found');
        }

        return View::make('admin.inquiries.edit', compact('inquiry'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validation = \Inquiry::validate(Input::all(),$id);
        if ($validation->fails()){
            return Redirect::route('admin.inquiries.update')
                ->withInput()
                ->withErrors($validation->messages()->all());
        }else{
            
            $inquiry = $this->inquiry->find($id);
            $inquiry->update(Input::all());
            return Redirect::route('admin.inquiries.show', $id)->with('message', 'update success.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->inquiry->find($id)->delete();

        return Redirect::route('admin.inquiries.index')->with('message', 'Delete success.');
    }

}
