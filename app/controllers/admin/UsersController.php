<?php
namespace Admin;
use View;
use Input;
use Validator;
use Hash;
use Redirect;

class UsersController extends \BaseController {

    /**
     * Post Repository
     *
     * @var Post
     */
    protected $user;

    public function __construct(\User $user)
    {
        $this->beforeFilter('csrf', array('on' => array('post')));
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$users = $this->user->all();
        $users = $this->user->paginate(100);
        //$class = $users->first()->getTable();
        //echo $class;die;
        //get_called_class();

        return View::make('admin.users.index', compact('users'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = \User::validate(Input::except("_token"));
        //dd($validation->messages()->all());die;

        if ($validation->fails()){
            return Redirect::route('admin.users.create')
                ->withInput()
                ->withErrors($validation->messages()->all());
        }else{
            $u = array(
                'username' => Input::get('username'),
                'password' => Hash::make(Input::get('password')),
                'email'    => Input::get('email'),
                'description' => Input::get('description')
            );
            $this->user->create($u);
            return Redirect::route('admin.users.index')->with('message', 'success.');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->user->findOrFail($id);

        return View::make('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->user->find($id);

        if (is_null($user))
        {
            return Redirect::route('admin.users.index')->with('message', 'User not found');
        }

        return View::make('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::only('description','status');
        $user = $this->user->find($id);
        $user->update($input);
        return Redirect::route('admin.users.show', $id)->with('message', 'update success.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->user->find($id)->delete();

        return Redirect::route('admin.users.index')->with('message', 'Delete success.');
    }

    public function switchStatus($id)
    {
        $user = $this->user->find($id);
        $status = ($user->status == ACTIVE) ? INACTIVE : ACTIVE ;
        $user->update(array('status' => $status));
        return \Response::json(array('code' => 'ok', 'message' => "<span class='label label-success'>".\User::$arr_status[$status]."</span>",'id'=>'status_'.$id));
    }

    public function resetPass($id)
    {
        $user = $this->user->find($id);
        $user->update(array('password' => Hash::make('sms123456')));
        return \Response::json(array('code' => 'ok','message' => null));
    }

}
