<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class PlansController extends \BaseController {

    /**
     * Post Repository
     *
     * @var Post
     */
    protected $plan;

    public function __construct(\Plan $plan)
    {
        //$this->beforeFilter('csrf', array('on' => array('post')));
        $this->plan = $plan;
    }

    public function editPlan($id,$category_id,$plan_id){
        $plan = $this->plan->where('facility_id','=',$id)
                            ->where('category_id','=',$category_id)
                            ->where('id','=',$plan_id)
                            ->first();
        $dataView = (String) View::make('admin.plans.edit',compact('plan'));                  

        return \Response::json(array('code' => '1'
                                    ,'message' => $dataView
                                ));
    }

    public function updatePlan($id,$category_id,$plan_id){
        $plan = $this->plan->where('facility_id','=',$id)
                            ->where('category_id','=',$category_id)
                            ->where('id','=',$plan_id)
                            ->first();
        $plan->update(Input::only('plan_name','plan_code','plan_unit','plan_description'));                    

        return \Response::json(array('code' => '1'
                                    ,'message' => array(
                                        'name'=>Input::get('plan_name'),
                                        'code'=>Input::get('plan_code'),
                                        'unit'=>Input::get('plan_unit'),
                                        'des'=>Input::get('plan_description'),
                                        'id'=>$plan_id
                                    )
                                ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deletePlan($id,$category_id,$plan_id){

        $plan = $this->plan->where('facility_id','=',$id)
                            ->where('category_id','=',$category_id)
                            ->where('id','=',$plan_id)
                            ->first();
        if (!is_null($plan)){
            $plan->delete();
        }

        return \Response::json(array('code' => '1'
                                    ,'message' => null
                                ));
    }

}
