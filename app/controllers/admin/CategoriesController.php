<?php
/**
 * Created by PhpStorm.
 * User: ducnn
 * Date: 2014-07-04
 * Time: 13:17
 */
namespace Admin;
use View;
use Input;
use Validator;
use Hash;
use Redirect;

class CategoriesController extends \BaseController {

    /**
     * Post Repository
     *
     * @var Post
     */
    protected $category;

    public function __construct(\Category $category)
    {
        $this->beforeFilter('csrf', array('on' => array('post')));
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = $this->category->paginate(100);
        return View::make('admin.categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = \Category::validate(Input::all());

        if ($validation->fails()){
            return Redirect::route('admin.categories.create')
                ->withInput()
                ->withErrors($validation);
        }else{
            $u = array(
                'name' => Input::get('name'),
                'description' => Input::get('description'),
                'create_user' => \Auth::user()->username,
                'update_user' => \Auth::user()->username
            );
            $this->category->create($u);
            return Redirect::route('admin.categories.index')->with('message', 'success.');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $category = $this->category->findOrFail($id);

        return View::make('admin.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->category->find($id);

        if (is_null($category))
        {
            return Redirect::route('admin.categories.index')->with('message', 'Category not found');
        }

        return View::make('admin.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $validation = \Category::validate(Input::all(),$id);

        if ($validation->fails()){
            return Redirect::route('admin.categories.edit', $id)
                ->withInput()
                ->withErrors($validation);
        }else{
            $input = Input::only('name','description');
            $category = $this->category->find($id);
            $category->update_user = \Auth::user()->username;
            $category->update($input);
            return Redirect::route('admin.categories.show', $id)->with('message', 'update success.');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->category->find($id)->delete();

        return Redirect::route('admin.categories.index')->with('message', 'Delete success.');
    }

}