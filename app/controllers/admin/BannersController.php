<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class BannersController extends \BaseController {

    /**
     * Post Repository
     *
     * @var Post
     */
    protected $banner;

    public function __construct(\Banner $banner)
    {
        $this->beforeFilter('csrf', array('on' => array('post')));
        $this->banner = $banner;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $banners = $this->banner->paginate(15);

        return View::make('admin.banners.index', compact('banners'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = \Banner::validate(Input::except("_token"));
        $validation2 = Validator::make(array('image'=>Input::file('image')),
		                              array('image'=>'required|image|mimes:jpg,jpeg,png,gif')
								     );
        if ($validation->fails()){
            return Redirect::route('admin.banners.create')
                ->withInput()
                ->withErrors($validation->messages()->all());
        }elseif ($validation2->fails()){
        	return Redirect::route('admin.banners.create')
                ->withInput()
                ->withErrors($validation2->messages()->all());
        }else{
            $obBanner = $this->banner->create(Input::except("_token","image"));
            $obImage = new \CImage('banners');
            $url = $obImage->upload(Input::file('image'),$obBanner->id);
            $obBanner->update(array('image_url'=>$url));

            return Redirect::route('admin.banners.index')->with('message', 'success.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $banner = $this->banner->findOrFail($id);

        return View::make('admin.banners.show', compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $banner = $this->banner->find($id);

        if (is_null($banner))
        {
            return Redirect::route('admin.banners.index')->with('message', 'banner not found');
        }

        return View::make('admin.banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
    	$validation = \Banner::validate(Input::except("_token","_method","image"));
        $validation2 = Validator::make(array('image'=>Input::file('image')),
		                              array('image'=>'image|mimes:jpg,jpeg,png,gif')
								     );
        if ($validation->fails()){
            return Redirect::route('admin.banners.edit',$id)
                ->withInput()
                ->withErrors($validation->messages()->all());
        }elseif ($validation2->fails()){
        	return Redirect::route('admin.banners.edit',$id)
                ->withInput()
                ->withErrors($validation2->messages()->all());
        }else{
            $obBanner = $this->banner->find($id);
            if(Input::hasFile('image'))
            {
            	$obImage = new \CImage('banners');
            	$url = $obImage->upload(Input::file('image'),$obBanner->id);
            	Input::merge(array('image_url'=> $url));
            }
            $obBanner->update(Input::except("_token","image","_method"));
            
            return Redirect::route('admin.banners.index')->with('message', 'success.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
    	$obImage = new \CImage('banners');
        $url 	 = $obImage->exeDelete($id);
        $this->banner->find($id)->delete();

        return Redirect::route('admin.banners.index')->with('message', 'Delete success.');
    }

}
