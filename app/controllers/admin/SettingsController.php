<?php
/**
 * Created by PhpStorm.
 * User: ducnn
 * Date: 2014-07-11
 * Time: 15:31
 */

namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class SettingsController extends \BaseController {

    /**
     * Post Repository
     *
     * @var Post
     */
    protected $setting;

    public function __construct(\Setting $setting)
    {
        $this->beforeFilter('csrf', array('on' => array('post')));
        $this->setting = $setting;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $settings = $this->setting->paginate(100);
        return View::make('admin.settings.index', compact('settings'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($setting_code)
    {
        $setting = $this->setting->find($setting_code);

        if (is_null($setting))
        {
            return Redirect::route('admin.settings.index')->with('message', 'Setting not found');
        }

        return View::make('admin.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($setting_code)
    {
       $validation = \Setting::validate(Input::all(),$setting_code);

       if ($validation->fails()){
           return Redirect::route('admin.settings.edit', $setting_code)
               ->withInput()
               ->withErrors($validation);
       }else{
            $input = Input::only('setting_name','description','value');
            $setting = $this->setting->find($setting_code);
            $setting->update_user = \Auth::user()->username;
            $setting->update($input);
            return Redirect::route('admin.settings.index')->with('message', 'Update success.');
        }


    }

}