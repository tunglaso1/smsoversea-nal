<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class ProvincesController extends \BaseController {

	protected $province;

    public function __construct(\Province $province)
    {
        $this->beforeFilter('csrf', array('on' => array('post')));
        $this->province = $province;
    }

    public function index()
    {
    	//$p =  $this->province->first();
    	//$u = $p->languagecontents()->first()->content;
    	//cdebug($u);die;
        \Province::setLang(null);
    	$provinces = $this->province->with('languagecontents')->get();
        return View::make('admin.provinces.index',compact('provinces'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.provinces.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $arr_check = Array(
                        Input::get(LANG_ENGLISH.'.name')
                       //,Input::get(LANG_VIETNAM.'.name')
                       ,Input::get(LANG_THAILAN.'.name') 
                     );
        if (!check_not_empty_group($arr_check)){
            return Redirect::route('admin.provinces.create')
                ->withInput()
                ->withErrors(array('name can not empty'));
        }else{
            if (!Input::has("major.is_view_district")){
                $arr_temp = Input::get('major');
                
                if (is_array($arr_temp)){
                    $arr_temp =  array_add($arr_temp, 'is_view_district', '0');
                }else{
                    $arr_temp = array("is_view_district" => '0');
                }
                Input::merge(array(
                                'major'=> $arr_temp
                            )
                );
            }
            $inputs = Input::all();
            $p  = $this->province->csave($inputs);
            return Redirect::route('admin.provinces.index')->with('message', 'success.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $province = $this->province->findOrFail($id);

        return View::make('admin.provinces.show', compact('province'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $province = $this->province->find($id);

        if (is_null($province))
        {
            return Redirect::route('admin.provinces.index')->with('message', 'province not found');
        }

        return View::make('admin.provinces.edit', compact('province'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $arr_check = Array(
                        Input::get(LANG_ENGLISH.'.name')
                       //,Input::get(LANG_VIETNAM.'.name')
                       ,Input::get(LANG_THAILAN.'.name') 
                     );
        if (!check_not_empty_group($arr_check)){
            return Redirect::route('admin.provinces.edit',$id)
                ->withInput()
                ->withErrors(array('name can not empty'));
        }else{
            if (!Input::has("major.is_view_district")){
                $arr_temp = Input::get('major');

                if (is_array($arr_temp)){
                    $arr_temp =  array_add($arr_temp, 'is_view_district', '0');
                }else{
                    $arr_temp = array("is_view_district" => '0');
                }
                Input::merge(array(
                                'major'=> $arr_temp
                            )
                );
            }
            $inputs = Input::all();
            $p  = $this->province->cupdate($inputs,$id);
            return Redirect::route('admin.provinces.index')->with('message', 'update success.');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //dd( $this->province->find($id));die;
        $this->province->cdelete($id);

        return Redirect::route('admin.provinces.index')->with('message', 'Delete success.');
    }

    
} 