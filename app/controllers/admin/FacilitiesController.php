<?php
namespace Admin;
use View;
use Input;
use Validator;
use Redirect;

class FacilitiesController extends \BaseController {

    /**
     * Post Repository
     *
     * @var Post
     */
    protected $facility;

    public function __construct(\Facility $facility)
    {
        $this->beforeFilter('csrf', array('on' => array('post'),'except' => array('switchStatus','addNewPlan')));
        $this->facility = $facility;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categories = array('0'=>'') + \Category::orderBy('name')->lists('name','id');
        $fa         = Input::get('fa');
        $fana       = Input::get('fana');
        //check filter or normal
        $facilities = $this->facility->with('categories');
        if ($fa){
            $facilities = $facilities->where('id','=',$fa);
        }
        if ($fana){
            $facilities = $facilities->where('name','like',$fana.'%');
        }
        if ($fa && $fana){
            $facilities = $facilities->get();
        }
        $facilities = $facilities->paginate(30); 
        
        return View::make('admin.facilities.index', compact('facilities','categories','fa','fana'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('admin.facilities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $validation = \Facility::validate(Input::all());

        if ($validation->fails()){
            return Redirect::route('admin.facilities.create')
                ->withInput()
                ->withErrors($validation);
        }else{
            $u = array(
                'name' => Input::get('name'),
                'description' => Input::get('description'),
                'create_user' => \Auth::user()->username,
                'update_user' => \Auth::user()->username
            );
            $this->facility->create($u);
            return Redirect::route('admin.facilities.index')->with('message', 'success.');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $facility = $this->facility->findOrFail($id);

        return View::make('admin.facilities.show', compact('facility'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    //facility tab
    public function edit($id)
    {
        $facility = $this->facility->find($id);
        $company[] = $facility->company;
        $tab  = 'facility';

        if (is_null($facility))
        {
            return Redirect::route('admin.facilities.index')->with('message', 'Facility not found');
        }

        return View::make('admin.facilities.edit', compact('facility','company','tab'));
    }

    public function getCategory($id)
    {
        $facility = $this->facility->find($id);
        $categories = \Category::orderBy('name')->get();
        $arr_categories = $facility->categories->lists('id');
        $tab  = 'category';

        if (is_null($facility))
        {
            return Redirect::route('admin.facilities.index')->with('message', 'Facility not found');
        }

        return View::make('admin.facilities.category_form', compact('id','categories','arr_categories','tab'));
    }

    public function getTimeable($id)
    {
        $facility = $this->facility->find($id);
        $tab  = 'timeable';

        if (is_null($facility))
        {
            return Redirect::route('admin.facilities.index')->with('message', 'Facility not found');
        }

        return View::make('admin.facilities.timeable_form', compact('facility','tab'));
    }

    public function getAccess($id)
    {
        die("Access");
    }

    public function getService($id)
    {
        $facility = $this->facility->find($id);
        $tab  = 'service';
        $categories = $facility->categories->lists('name','id');
        if ($categories){
            if (Input::has('category_id')){
                $category_first_id = Input::get('category_id');
            }else{
                reset( $categories );
                $category_first_id = key( $categories );
            }
            
            $faciCategory  = \FacilityCategory::where('facility_id','=',$facility->id)
                            ->where('category_id','=',$category_first_id)
                            ->first();
            $plans = \Plan::where('facility_id','=',$facility->id)
                            ->where('category_id','=',$category_first_id)
                            ->get();
        }
        

        if (is_null($facility))
        {
            return Redirect::route('admin.facilities.index')->with('message', 'Facility not found');
        }

        return View::make('admin.facilities.service_form', compact('facility','tab','categories','plans','category_first_id','faciCategory'));
    }

    public function getGallery($id)
    {
        die("Gallery");
    }

    public function getContact($id)
    {
        $facility = $this->facility->find($id);
        $tab  = 'contact';

        if (is_null($facility))
        {
            return Redirect::route('admin.facilities.index')->with('message', 'Facility not found');
        }

        return View::make('admin.facilities.contact_form', compact('facility','tab'));
    }

    public function getOther($id)
    {
        die("Other");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();
        $validation = \Facility::validate($input);
        $validationComp = \Facility::validateCompany($input['company']);
        $validationImage = Validator::make(array('image'=>Input::file('image')),
                                           array('image'=>'image|mimes:jpg,jpeg,png,gif')
                                     );

        if ($validation->fails() || $validationComp->fails() || $validationImage->fails()){
            //check validation
            $arrErrorMix = array_merge_recursive(
                                $validation->messages()->toArray(), 
                                $validationComp->messages()->toArray(),
                                $validationImage->messages()->toArray()
                            );
            return Redirect::route('admin.facilities.edit',array($id,'tab'=>'facility'))
                ->withInput()
                ->withErrors($arrErrorMix);
        }else{
            // process after check validate
            $input = Input::except('_method','_token','image');
            $facility = $this->facility->find($id);
            $result = $facility->saveAllRelation($input);
            if (Input::hasFile('image'))
            {
                $obImage = new \CImage('facility');
                $url = $obImage->upload(Input::file('image'),$result->id);
                $result->update(array('facility_image'=>$url)); 
            }

            return Redirect::route('admin.facilities.edit',$result->id)->with('message', 'success.');
        }
    }

    public function updateCategories($id)
    {
        $arr_categories = Input::get('category');
        $facility = $this->facility->find($id);
        //\Log::info($arr_categories);
        if (!is_null($facility))
        {
            $facility->categories()->sync($arr_categories);
        }
        return Redirect::route('admin.facilities.getCategory',$facility->id)->with('message', 'success.');
    }

    public function updateService($id)
    {
        $category_id = Input::get('category_id');
        $facilityCategory = \FacilityCategory::where('facility_id','=',$id)
                            ->where('category_id','=',$category_id)
                            ->first();
        $input  = Input::only('basic_service_avaiable','basic_service_notavaiable');
        $facilityCategory->update($input);

        return Redirect::route('admin.facilities.getService',array($id, 'category_id'=>$category_id))->with('message', 'success.');
    }

    public function updateTimeable($id)
    {
        $timeable = Input::get('daily_timeable');
        $facility = $this->facility->find($id);
        if (!is_null($facility))
        {
            $facility->update(array('daily_timeable'=>$timeable));
        }
        return Redirect::route('admin.facilities.getTimeable',$facility->id)->with('message', 'success.');
    }

    public function updateGallery($id)
    {
        die("Gallery");
    }

    public function updateAccess($id)
    {
        die("Access");
    }

    public function updateContact($id)
    {
        $faci = Input::except('_method','_token');
        $facility = $this->facility->find($id);
        if (!is_null($facility))
        {
            $facility->update($faci);
        }
        return Redirect::route('admin.facilities.getContact',$facility->id)->with('message', 'success.');
    }

    public function updateOther($id)
    {
        die("Other");
    }

    public function switchStatus($id)
    {
        $facility = $this->facility->find($id);
        $status = ($facility->status == ACTIVE) ? INACTIVE : ACTIVE ;
        $facility->update(array('status' => $status));
        return \Response::json(array('code' => 'ok'
                                    ,'message' => "<span class='label label-success'>".\Facility::$arr_status[$status]."</span>"
                                    ,'id'=>'status_'.$id
                                ));
    }

    public function addNewPlan($id,$category_id)
    {
       $plan = \Plan::create(array(
            'facility_id'=>$id,
            'category_id'=>$category_id,
            'plan_name'=>Input::get('plan_name'),
            'plan_code'=>Input::get('plan_cost'),
            'plan_unit'=>Input::get('plan_unit'),
            'plan_description'=>Input::get('plan_description')
        ));
       $row = "<tr><td>".$plan->plan_name."</td><td>".$plan->plan_code."</td><td>".$plan->plan_unit."</td><td>".\Str::limit($plan->plan_description, 80)."</td><td><a href='".route('admin.plans.updatePlan',array($id,$plan->category_id,$plan->id))."' plan-edit='true'> Edit</a><a href='".route('admin.plans.deletePlan',array($id,$plan->category_id,$plan->id))."' plan-delete='true'> Delete</a></td></tr>";
       return \Response::json(array('code' => '1'
                                    ,'message' => $row
                                ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->facility->find($id)->delete();

        return Redirect::route('admin.facilities.index')->with('message', 'Delete success.');
    }

}