@extends('layouts.backend')

@section('content')

<div class="row">
     <!-- page header -->
    <div class="col-lg-12">
        <h1 class="page-header">Change Pass</h1>
    </div>
    <!--end page header -->
</div>

	<div class="col-lg-6">
	{{ Form::open(array('url'=>App::getLocale().'/admin/changePass', 'role'=>'form')) }}
	    <ul>
		@if (Session::has('flash_error'))
		   <li id="flash_error">{{ Session::get('flash_error') }}</li>
	    @endif

        @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
	    </ul>
		
		<div class="form-group">
			<label>Old Pass </label>
			{{ Form::password('old_password', array('class'=>'form-control', 'placeholder'=> 'Old Pass')) }}
		</div>
		
		<div class="form-group">
			<label>New Pass </label>
			{{ Form::password('password', array('class'=>'form-control', 'placeholder'=> 'New Pass')) }}
		</div>
		
		<div class="form-group">
			<label> Confirmation </label>
			{{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=> 'Confirm' )) }}
		</div>
		
		
		<div class="form-action">
			{{ Form::submit('Save' , array('class'=>'btn btn-primary'))}}
		</div>
	
	    
	{{ Form::close() }}

	</div>

@stop
