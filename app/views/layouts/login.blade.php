<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SMS| Admin</title>
    <!-- Core CSS - Include with every page -->
    {{ stylesheet_link_tag('bootstrap') }}
    {{ stylesheet_link_tag('style') }}
    {{ stylesheet_link_tag('main-style') }}

</head>

<body class="body-Login-back">
    <!--  container -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 text-center logo-margin "></div>

            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">                  
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        @if(Session::has('flash_error'))
                            <p class="alert">{{ Session::get('flash_error') }}</p>
                        @endif
                        
                        {{ Form::open(array('url'=>App::getLocale().'/admin/login', 'class'=>'form-signin','role'=>'form')) }}
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail Or Username" name="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="1">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                {{ Form::submit('Login', array('class'=>'btn btn-lg btn-success btn-block'))}}
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end container -->

</body>

</html>
