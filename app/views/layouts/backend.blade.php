<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>SMS | Admin</title>
    <!-- Core CSS - Include with every page -->
    {{ stylesheet_link_tag('bootstrap') }}
    {{ stylesheet_link_tag('font-awesome') }}
    {{-- stylesheet_link_tag('pace-theme-big-counter') --}}
    {{ stylesheet_link_tag('style') }}
    {{ stylesheet_link_tag('main-style') }}
    {{-- stylesheet_link_tag('dataTables.bootstrap') --}}
    {{ stylesheet_link_tag("jquery_datetimepicker") }}
    @yield('scriptsck')

</head>

<body>
    <!--  wrapper -->
    <div id="wrapper">
        <!-- navbar top -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" id="navbar">
            <!-- navbar-header -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    {{ HTML::image('assets/img/logo.png', $alt="", $attributes = array()) }}
                </a>
            </div>
            <!-- end navbar-header -->
            <!-- navbar-top-links -->
            <ul class="nav navbar-top-links navbar-right">
                <!-- <li>
                    {{ Form::open(array('url' => '/change_locale', 'method' => 'post' , 'onchange' => 'this.submit();')) }}
                        {{ Form::select('set_locale', array('en' => trans('admin.english'),'th' => trans('admin.thailand')),Request::segment(1),array('class'=>'select input-sm')) }}
                    {{ Form::close() }}
                </li> -->
                <!-- main dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-user fa-3x"></i></a>
                    <!-- dropdown user-->
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i>User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ route('users.changePass') }}"><i class="fa fa-gear fa-fw"></i>Change Pass</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{ "/".App::getLocale().'/admin/logout' }}"><i class="fa fa-sign-out fa-fw"></i>{{trans('admin.logout')}}</a>
                        </li>
                    </ul>
                    <!-- end dropdown-user -->
                </li>
                <!-- end main dropdown -->
            </ul>
            <!-- end navbar-top-links -->
            

        </nav>
        <!-- end navbar top -->

        <!-- navbar side -->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <!-- sidebar-collapse -->
            <div class="sidebar-collapse">
                <!-- side-menu -->
                <ul class="nav" id="side-menu">
                    <!-- <li> -->
                        <!-- user image section-->
                       <!--  <div class="user-section">
                            <div class="user-section-inner">
                                <img src="/assets/user.jpg" alt="">
                            </div>
                            <div class="user-info">
                                <div>Jonny <strong>Deen</strong></div>
                                <div class="user-text-online">
                                    <span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;Online
                                </div>
                            </div>
                        </div> -->
                        <!--end user image section-->
                    <!-- </li> -->
                   
                    <li class="{{HTML::activeState('admin\/dashboard')}}">
                        <a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard fa-fw"></i>{{ trans('admin.dashboard') }}</a>
                    </li>

                    <li class="{{HTML::activeState('admin\/users')}}">
                        <a href="{{route('admin.users.index')}}"><i class="fa fa-user fa"></i>{{trans('admin.user')}}</a>
                        <!-- second-level-items -->
                    </li>

                    <li class="{{HTML::activeState('admin\/banners')}}">
                        <a href="{{route('admin.banners.index')}}"><i class="fa fa-table fa-fw"></i>Banner</a>
                    </li>

                    <li class="{{HTML::activeState('admin\/statics')}}">
                        <a href="{{route('admin.pages.index')}}"><i class="fa fa-edit fa-fw"></i>Static Page</a>
                    </li>

                    <li class="{{HTML::activeState('admin\/filters')}}">
                        <a href="{{route('admin.filters.index')}}"><i class="fa fa-edit fa-fw"></i>Search Filter</a>
                    </li>

                    <li class="{{HTML::activeState('admin\/inquiries')}}">
                        <a href="{{route('admin.inquiries.index')}}"><i class="fa fa-edit fa-fw"></i>Inquiries</a>
                    </li>

                    <li class="{{HTML::activeState('admin\/facilities')}}">
                        <a href="{{route('admin.facilities.index')}}"><i class="fa fa-flask fa-fw"></i>{{ trans('admin.facilities')}}</a>
                    </li>

                    <li class="{{HTML::activeState('admin\/settings')}}">
                        <a href="{{route('admin.settings.index')}}"><i class="fa fa-wrench fa-fw"></i>{{ trans('admin.setting') }}</a>
                        <!-- second-level-items -->
                    </li>
                    <li id="li_master">
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i>{{ trans('admin.master') }}<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li class="{{HTML::activeState('admin\/categories')}}">
                                <a href="{{route('admin.categories.index')}}">{{ trans('admin.category')}}</a>
                            </li>
                            <li class="{{HTML::activeState('admin\/provinces')}}">
                                <a href="{{route('admin.provinces.index')}}">Province | District</a>
                            </li>
                            <li>
                                <a href="#">Third Level Item</a>
                            </li>
                        </ul>
                        <!-- second-level-items -->
                    </li>
                    
                </ul>
                <!-- end side-menu -->
            </div>
            <!-- end sidebar-collapse -->
        </nav>
        <!-- end navbar side -->
        <!--  page-wrapper -->
        <div id="page-wrapper">
            @if(Session::has('message'))
                <div class="alert alert-success">{{ Session::get('message') }}</div>
            @endif
            @yield('content')

        </div>
        <!-- end page-wrapper -->

    </div>
    <!-- end wrapper -->

    <!-- Core Scripts - Include with every page -->
    {{ javascript_include_tag('jquery-1-10-2') }}
    {{ javascript_include_tag('bootstrap-min') }}
    {{ javascript_include_tag('jquery-metisMenu') }}
    {{ javascript_include_tag('pace') }}
    {{ javascript_include_tag('siminta') }}
    {{-- javascript_include_tag('jquery.dataTables') --}}
    {{-- javascript_include_tag('dataTables.bootstrap') --}}
    {{ javascript_include_tag('jquery_datetimepicker') }}
    
    <script>
    $( document ).ready(function() {
        $( "input[data-confirm='true']" ).click(function (e) { // binding onclick
            var answer = confirm('Are you sure you want to delete this?');
            if (answer)
            {
              return true;
            }
            else
            {
              e.preventDefault();
              return false;
            }
         });
    });
    </script>

    <script>
    $('.datepicker').datetimepicker({
        changeYear:true,
        lang:'en',
        timepicker:false,
        format:'Y-m-d',
        formatDate:'Y-m-d'
    });
    $('.datetime_picker').datetimepicker({
        changeYear:true,
        lang:'ja',
        format:'Y-m-d H:i:s',
        formatDate:'Y-m-d'
    });
    </script>

    <script>
    $( document ).ready(function() {
            setTimeout(function() {
                  $( "div.alert-success" ).remove();
            }, 2000);
    });
    </script>

    <script>
    $( document ).ready(function() {
            var obExist = $('ul.nav.nav-second-level').find('li.selected');
            if (obExist.size())
            {
                console.log('vo he day');
                $('li#li_master > a').css( "color", "#EEA236" );
            }
    });
    </script>
    @yield('scripts')

</body>

</html>
