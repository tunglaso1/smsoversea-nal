<!-- @extends('layouts.scaffold')
 -->
@section('main')

<h1>All Book</h1>

<p>{{ link_to_route('books.create', 'Add New Book', null, array('class' => 'btn btn-lg btn-success')) }}</p>

@if ($books->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Category</th>
				<th>Des</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($books as $book)
				<tr>
					<td>{{{ $book->name }}}</td>
					<td>{{{ $book->category }}}</td>
					<td>{{{ $book->des }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('books.destroy', $book->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('books.edit', 'Edit', array($book->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no books
@endif

@stop
