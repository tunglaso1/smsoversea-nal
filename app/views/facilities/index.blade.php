<?php
?>

@extends('layouts.scaffold')

@section('main')

<div class="text-right">
{{ Form::open(array('url' => '', 'class' => 'form-horizontal')) }}
    {{ Form::select('lang', array('en' => 'en', 'vn' => 'vn')) }}
{{ Form::close() }}
</div>

<h1>All Facilities</h1>

<p>{{ link_to_route('facilities.create', 'Add New Facility', null, array('class' => 'btn btn-lg btn-success')) }}</p>

<div class="container text-center">
{{ Form::open(array('url' => 'facilities/search')) }}
    {{ Form::text('name', isset($input) ? $input['name'] : null, array('placeholder' => 'Name')) }}
    {{ Form::select('province', array('' => '--- Select Province ---', 'Hà Nội' => 'Hà Nội', "Hồ Chí Minh" => "Hồ Chí Minh" ), isset($input) ? $input['province'] : null) }}

    @if ($filters->count())
        @foreach ($filters as $filter)
            <div>
                {{ Form::label($filter->criteria, $filter->criteria) }}
                {{ Form::text($filter->criteria, isset($input) ? $input[$filter->criteria] : null, array('placeholder'=>$filter->criteria)) }}
            </div>
        @endforeach
    @endif

    {{ Form::submit('Search', array('class' => 'btn btn-danger')) }}
{{ Form::close()}}
</div>

@if ($facilities->count())
	<table class="table table-striped">
		<thead>
			<tr>
				<th>Name</th>
				<th>Price</th>
				<th>Province</th>
				<th>Rate</th>
				<th>&nbsp;</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($facilities as $facility)
				<tr>
					<td>{{{ $facility->name }}}</td>
					<td>{{{ $facility->price }}}</td>
					<td>{{{ $facility->province }}}</td>
					<td>{{{ $facility->rate }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('facilities.destroy', $facility->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('facilities.edit', 'Edit', array($facility->id), array('class' => 'btn btn-info')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no facilities
@endif

@stop
