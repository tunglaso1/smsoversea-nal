@extends('layouts.scaffold')

@section('main')

<h1>Show Facility</h1>

<p>{{ link_to_route('facilities.index', 'Return to All facilities', null, array('class'=>'btn btn-lg btn-primary')) }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Name</th>
				<th>Price</th>
				<th>Province</th>
				<th>Rate</th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $facility->name }}}</td>
					<td>{{{ $facility->price }}}</td>
					<td>{{{ $facility->province }}}</td>
					<td>{{{ $facility->rate }}}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('facilities.destroy', $facility->id))) }}
                            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                        {{ link_to_route('facilities.edit', 'Edit', array($facility->id), array('class' => 'btn btn-info')) }}
                    </td>
		</tr>
	</tbody>
</table>

@stop
