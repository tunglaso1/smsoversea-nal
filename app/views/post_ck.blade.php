<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Full Page Editing &mdash; CKEditor Sample</title>
	
</head>
<body>
	{{ $content1  }}
	<div id="footer">
		<hr>
		<p>
			CKEditor - The text editor for the Internet - <a class="samples" href="http://ckeditor.com/">http://ckeditor.com</a>
		</p>
		<p id="copy">
			Copyright &copy; 2003-2014, <a class="samples" href="http://cksource.com/">CKSource</a> - Frederico
			Knabben. All rights reserved.
		</p>
	</div>
			

</body>
</html>
