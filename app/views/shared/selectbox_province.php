<?php
	$provinces = Province::with('languagecontents')->get();
	$obs = array();
	foreach ($provinces as $province)
	{
		$obs[$province->id] => $province->languagecontents()->first()->content;
	}
?>
<div class="form-group">
    {{ Form::label('province_id', 'Provinces', array('class'=>'col-md-2 control-label')) }}
    <div class="col-sm-6">
        {{ Form::select('major[province_id]', $provinces, $id_p) }}
    </div>
</div>