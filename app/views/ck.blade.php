<!DOCTYPE html>
<html>
    <head>
        <title>A Simple Page with CKEditor</title>
        <!-- Make sure the path to CKEditor is correct. -->
        <script src="assets/ckeditor/ckeditor.js"></script>
    </head>
    <body>
        <form>
            <textarea name="editor1" id="editor1" rows="10" cols="80">
                This is my textarea to be replaced with CKEditor.
            </textarea>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1',
				{
				filebrowserBrowseUrl : 'assets/ckeditor/ckfinder/ckfinder.html',
				filebrowserImageBrowseUrl : 'assets/ckeditor/ckfinder/ckfinder.html?type=Images',
				filebrowserFlashBrowseUrl : 'assets/ckeditor/ckfinder/ckfinder.html?type=Flash',
				filebrowserUploadUrl : 'assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
				filebrowserImageUploadUrl : 'assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
				filebrowserFlashUploadUrl : 'assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
				} 
				);
            </script>
        </form>
    </body>
</html>