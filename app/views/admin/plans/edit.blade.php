<div class="modal fade" id="myModalEdit" tabindex="-2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Edit Plan</h4>
                </div>
                <div class="modal-body">
        {{ Form::model($plan, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.plans.editPlan', $plan->facility_id,$plan->category_id,$plan->id))) }}

        <div class="form-group">
            {{ Form::label('name', 'Plan Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
                {{ Form::text('plan_name',null, array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('cost', 'Cost:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('plan_code', null, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('unit', 'Unit:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('plan_unit', null, array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('remark', 'Remark:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::textarea('plan_description', null, array('class'=>'form-control')) }}
            </div>
        </div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" id="edit_plan_row">Save changes</button>
</div>

{{ Form::close() }}
                </div>
                
            </div>
        </div>
    </div>

<script>
$('button#edit_plan_row').on("click", function(e) {
        e.preventDefault();
        var dataForm = $(this).closest("form").serialize();
        var url = $(this).closest("form").attr('action');
        console.log(dataForm);

        $.ajax({
             type: "patch",
             url : url,
             data : dataForm,
             success : function(data){
                if (data['code'] == '1'){
                    console.log(data['message']);
                    $("div#myModalEdit").modal('hide');
                    $('div.form-edit-dialog').html();
                    var crow = $('tr#row_'+data['message']['id']+' td');
                    //console.log(crow.eq(0).html('aa'));
                    crow.eq(0).html(data['message']['name']); 
                    crow.eq(1).html(data['message']['code']); 
                    crow.eq(2).html(data['message']['unit']); 
                    crow.eq(3).html(data['message']['des']); 
                }
             }
          },"json");
     });
     //
</script>
