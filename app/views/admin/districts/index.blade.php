@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Districts Management
            <div style="float:right;">{{ link_to_route('admin.provinces.districts.create', 'Add New Province', $id_p, array('class' => 'label label-success')) }}</div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($districts->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>Name Eng</th>
				<th>Name Thai</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($districts as $district)
				<tr>
					@foreach ($district->languagecontents as $name)
				  		<td>
				  			@unless (is_null($name))
				  				{{ $name->content }}
				  			@endunless
				  		</td>
					@endforeach
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.provinces.districts.destroy',$id_p, $district->id))) }}
                            {{ Form::submit('Delete', array('class' => 'label-warning','data-confirm'=>'true')) }}
                        {{ Form::close() }}
                        {{ link_to_route('admin.provinces.districts.edit', 'Edit', array($id_p,$district->id), array('class' => 'label label-success')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no districts
@endif
				</div>
			</div>
		</div>
		
	</div>
</div>


@stop
