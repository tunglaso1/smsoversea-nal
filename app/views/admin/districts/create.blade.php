@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Create District
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::open(array('route' => 'admin.provinces.districts.store', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label(LANG_ENGLISH.'[name]', 'English Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text(LANG_ENGLISH.'[name]', null, array('class'=>'form-control')) }}
            </div>
        </div>

        <!-- <div class="form-group">
            {{ Form::label(LANG_VIETNAM.'[name]', 'Vietnam Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text(LANG_VIETNAM.'[name]', null, array('class'=>'form-control')) }}
            </div>
        </div> -->

        <div class="form-group">
            {{-- Form::label('name_'.THAILAN, 'Thailan Name:', array('class'=>'col-md-2 control-label')) --}}
            {{ Form::label('name_'.THAILAN, 'Des:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text(LANG_THAILAN.'[name]', null, array('class'=>'form-control')) }}
            </div>
        </div>

        @include('shared.selectbox_province',array('id_p'=>$id_p))

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop


