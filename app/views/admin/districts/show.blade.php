@extends('layouts.backend')

@section('content')

<?php  $arr_active = array(ACTIVE => 'Active', INACTIVE => 'Inactive'); ?>

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i> User Detail
            <div style="float:right;">
            	{{ link_to_route('admin.categories.index', 'All Categories', null, array('class'=>'label label-success')) }}
        	</div>
        </div>
        <div class="panel-body">

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	<tr>
		<td>Username</td>
		<td>{{ $category->name }}</td>
	</tr>

	<tr>
		<td>Description</td>
		<td>{{ $category->description }}</td>
	</tr>

</table>
</div>
</div>
</div>

@stop
