@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Create User
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::model($setting, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.settings.update', $setting->setting_code))) }}

        <div class="form-group">
            {{ Form::label('setting_code', 'Setting Code:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
                {{$setting->setting_code}}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('setting_name', 'Setting Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
                {{ Form::text('setting_name',$setting->setting_name, array('class'=>'form-control', 'placeholder'=>'Setting Name')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('description', $setting->description, array('class'=>'form-control', 'placeholder'=>'Description')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('value', 'Value:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
                {{ Form::text('value', $setting->value, array('class'=>'form-control', 'placeholder'=>'Value')) }}
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-6">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.settings.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop