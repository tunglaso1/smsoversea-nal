@extends('layouts.backend')

@section('content')

<?php  $arr_active = array(ACTIVE => 'Active', INACTIVE => 'Inactive'); ?>

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>List Settings
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($settings->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>Setting Code</th>
				<th>Setting Name</th>
				<th>Descriptionr</th>
                <th>Value</th>
                <th>Created Use</th>
				<th>Updated User</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($settings as $setting)
				<tr>
					<td>{{ $setting->setting_code }}</td>
                    <td>{{ $setting->setting_name }}</td>
					<td>{{ Str::limit($setting->description, 80) }}</td>
                    <td>{{ $setting->value }}</td>
                    <td>{{ $setting->create_user }}</td>
                    <td>{{ $setting->update_user }}</td>
                    <td>
                        {{ link_to_route('admin.settings.edit', 'Edit', array($setting->setting_code), array('class' => 'label label-success')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no settings
@endif
				</div>
			</div>
		</div>
		@include('shared.pagination',array('obs'=>$settings))
	</div>
</div>


@stop
