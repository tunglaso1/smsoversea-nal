@extends('layouts.backend')

@section('content')

<?php  $arr_inquiries = array(INQUIRY_ONLY_SMS=>'Only SMS',INQUIRY_SMS_AND_VENDOR=>'SMS And Vendor'); ?>

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i> Inquiry Detail
            <div style="float:right;">
            	{{ link_to_route('admin.inquiries.index', 'All Inquiries', null, array('class'=>'label label-success')) }}
        	</div>
        </div>
        <div class="panel-body">

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	<tr>
		<td class="col-md-2">Type</td>
		<td>{{ $arr_inquiries[$inquiry->inquiry_type] }}</td>
	</tr>

	<tr>
		<td>Email</td>
		<td>{{ $inquiry->inquiry_email }}</td>
	</tr>

	<tr>
		<td>Mobile</td>
		<td>{{ $inquiry->inquiry_mobile }}</td>
	</tr>

	<tr>
		<td>Memory</td>
		<td>{{ $inquiry->memo }}</td>
	</tr>
	<tr>
		<td>Confirm Mail Status</td>
		<td>{{ $inquiry->confirm_mail_status }}</td>
	</tr>

	<tr>
		<td>Contetn</td>
		<td>{{ $inquiry->inquiry_content }}</td>
	</tr>

</table>
</div>
</div>
</div>

@stop
