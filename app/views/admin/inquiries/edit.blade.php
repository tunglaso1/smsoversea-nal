@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Create Inquiry
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::model($inquiry, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.inquiries.update', $inquiry->id))) }}

        <div class="form-group">
            {{ Form::label('inquiry_type', 'Inquiry Type:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::select('inquiry_type', array(INQUIRY_ONLY_SMS=>'Only SMS',INQUIRY_SMS_AND_VENDOR=>'SMS And Vendor'),$inquiry->inquiry_type) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('inquiry_email', 'Email:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('inquiry_email', Input::old('inquiry_email'), array('class'=>'form-control', 'placeholder'=>'Email')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('inquiry_mobile', 'Mobile:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('inquiry_mobile', Input::old('inquiry_mobile'), array('class'=>'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('memo', 'Memo:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('memo', Input::old('memo'), array('class'=>'form-control')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('confirm_mail_status', 'Status Confirm:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('confirm_mail_status', Input::old('confirm_mail_status'), array('class'=>'form-control', 'placeholder'=>'Confirm Mail Status')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('inquiry_content', 'Content:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::textarea('inquiry_content', Input::old('inquiry_content'), array('class'=>'form-control')) }}
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-6">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.inquiries.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop