@extends('layouts.backend')

@section('content')

<?php  $arr_inquiries = array(INQUIRY_ONLY_SMS=>'Only SMS',INQUIRY_SMS_AND_VENDOR=>'SMS And Vendor'); ?>

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>List Inquiries
            <div style="float:right;">{{ link_to_route('admin.inquiries.create', 'Add New Inquiry', null, array('class' => 'label label-success')) }}</div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($inquiries->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>Id</th>
				<th>Type</th>
				<th>Email</th>
				<th>Mobile</th>
				<th>Content</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($inquiries as $inquiry)
				<tr>
					<td>{{ $inquiry->id }}</td>
					<td>{{ $arr_inquiries[$inquiry->inquiry_type] }}</td>
					<td>{{ $inquiry->inquiry_email }}</td>
					<td>{{ $inquiry->inquiry_mobile }}</td>
					<td>{{ Str::limit($inquiry->inquiry_content, 80) }}</td>
                    <td>
                    	{{ link_to_route('admin.inquiries.show', 'Show', array($inquiry->id), array('class' => 'label label-success')) }}
                    	{{ link_to_route('admin.inquiries.edit', 'Edit', array($inquiry->id), array('class' => 'label label-success')) }}
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.inquiries.destroy', $inquiry->id))) }}
                            {{ Form::submit('Delete', array('class' => 'label-warning','data-confirm'=>'true')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no inquiry
@endif
				</div>
			</div>
		</div>
		@include('shared.pagination',array('obs'=>$inquiries))
	</div>
</div>


@stop
