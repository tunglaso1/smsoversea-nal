<ul class="nav nav-tabs">
    <li class="{{ $tab == 'category' ? 'active' : ''}}">
         <a href="{{route('admin.facilities.getCategory',$id)}}">Category</a>
    </li>
    <li class="{{ $tab == 'facility' ? 'active' : ''}}">
         <a href="{{route('admin.facilities.edit',$id)}}">Facility</a>
    </li>
    <li class="{{ $tab == 'service' ? 'active' : ''}}">
         <a href="{{route('admin.facilities.getService',$id)}}">Service & Fee</a>
    </li>
    <li class="{{ $tab == 'timeable' ? 'active' : ''}}">
         <a href="{{route('admin.facilities.getTimeable',$id)}}">Timeable</a>
    </li>
    <li class="{{ $tab == 'gallery' ? 'active' : ''}}">
         <a href="{{route('admin.facilities.getGallery',$id)}}">Gallery</a>
    </li>
    <li class="{{ $tab == 'access' ? 'active' : ''}}">
         <a href="{{route('admin.facilities.getAccess',$id)}}">Access</a>
    </li>
    <li class="{{ $tab == 'contact' ? 'active' : ''}}">
         <a href="{{route('admin.facilities.getContact',$id)}}">Contact</a>
    </li>
    <li><a href="#other_id" data-toggle="tab">Others</a>
    </li>
</ul>