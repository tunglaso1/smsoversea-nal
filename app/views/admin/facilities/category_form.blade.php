@extends('layouts.backend')

@section('content')
<div class="alert-list-error"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    </div>
@endif
<div class="col-lg-12">
     <!--Basic Tabs   -->
    <div class="panel panel-default">
        <div class="panel-body">
            @include('admin.facilities.navigation',array('id'=>$id,'tab'=>$tab))
            <div class="tab-content">
                {{ Form::open(array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.facilities.updateCategories', $id))) }}
                <br/>
                @foreach ($categories as $category)
                    <div class="form-group">
                        {{ Form::label('name', $category->name, array('class'=>'col-md-2 control-label')) }}
                        <div class="col-sm-6">
                            {{ Form::checkbox('category[]', $category->id, in_array($category->id, $arr_categories),array('class'=>'col-sm-1')) }}
                        </div>
                    </div>
                @endforeach
            
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-6">
                        {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
                        {{ link_to_route('admin.facilities.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <!--End Basic Tabs   -->
</div>

@stop