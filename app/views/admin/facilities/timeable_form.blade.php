@extends('layouts.backend')
@section('scriptsck')
    <script src="/assets/ckeditor/ckeditor.js"></script>
@stop

@section('content')
<div class="alert-list-error"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    </div>
@endif
<div class="col-lg-12">
     <!--Basic Tabs   -->
    <div class="panel panel-default">
           <div class="panel-body">
                @include('admin.facilities.navigation',array('id'=>$facility->id,'tab'=>$tab))

               <div class="tab-content">
                   <h4>Daily Timeable</h4>
                       {{ Form::model($facility, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.facilities.updateTimeable', $facility->id))) }}
                            <br/>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    {{ Form::textarea('daily_timeable', null, array('class'=>'form-control','rows'=>15,'id'=>'editor1')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-6">
                                    {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
                                    {{ link_to_route('admin.facilities.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
                                </div>
                            </div>
                        {{ Form::close() }}
               </div>

           </div>
    </div>
    <!--End Basic Tabs   -->
</div>

@stop

{{-- Scripts --}}
@section('scripts')
  <script type="text/javascript">
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
                CKEDITOR.replace( 'editor1',
                {
                filebrowserBrowseUrl : '/assets/ckeditor/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/assets/ckeditor/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/assets/ckeditor/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                } 
                );
    </script>
@stop