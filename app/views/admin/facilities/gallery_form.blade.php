@extends('layouts.backend')

@section('content')
<div class="alert-list-error"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    </div>
@endif
<div class="col-lg-12">
     <!--Basic Tabs   -->
    <div class="panel panel-default">
           <div class="panel-body">
                @include('admin.facilities.navigation',array('id'=>$facility->id,'tab'=>$tab))
               <div class="tab-content">
                   <div class="{{ $tab == 'facility' ? 'tab-pane fade in active' : 'tab-pane fade'}}" id="facility_id">
                        {{ Form::model($facility, array('class' => 'form-horizontal','files'=>true, 'method' => 'PATCH','skip'=>'1', 'route' => array('admin.facilities.update', $facility->id))) }}
                            <br/>
                            <div class="form-group">
                                {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('name', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label"> Image </label>
                                <div class="col-sm-6">
                                  {{ Form::file('image', null, array('class'=>'form-control')) }}
                                  {{ HTML::image($facility->getThumb(), $alt="") }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('person_in_charge', 'Persion In Charge', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('person_in_charge', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('open_date', 'Open Time:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('open_date', null, array('class'=>'datetime_picker')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('homepage', 'Homepage:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('homepage', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('facility_use_status', 'Status Use:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('facility_use_status', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('sale_message', 'Message:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::textarea('sale_message', null, array('class'=>'form-control','rows'=>3)) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('sum_area', 'Sum Area:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('sum_area', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('sum_room_area', 'Sum Room Area:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('sum_room_area', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('room_number', 'Room Number:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('room_number', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('private_room_number', 'Private Room Number:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('private_room_number', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('room_max_patien', 'Room Max Patien:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('room_max_patien', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('room_area', 'Room area:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('room_area', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('sharing_facility', 'Shareing Facility:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('sharing_facility', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('other_info', 'Other Info:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::textarea('other_info', null, array('class'=>'form-control','rows'=>3)) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('age', 'Age:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('age', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('healthy_level', 'Healthy Level:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('healthy_level', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('avaiable_ilness', 'Avaiable Ilness:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('avaiable_ilness', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('not_avaiable_ilness', 'Not Avaiable Ilness:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('not_avaiable_ilness', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('medical_treatment', 'Medical Treatment:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('medical_treatment', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <!--Company -->
                            <fieldset>
                              <legend>Company Info</legend>
                            <div class="form-group">
                                {{ Form::label('name', 'Company name:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('company[name]', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('person_in_charge', 'Persion In Charge:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('company[person_in_charge]', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('open_date', 'Open Date:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('company[open_date]', null, array('class'=>'datetime_picker')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('Adress', 'Address:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('company[address]', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('contact_info', 'Contact info:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('company[contact_info]', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('homepage', 'Homepage:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('company[homepage]', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            </fieldset>


                            <div class="form-group">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-6">
                                    {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
                                    {{ link_to_route('admin.facilities.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
                                </div>
                            </div>
                        {{ Form::close() }}
                   </div>
               </div>

           </div>
    </div>
    <!--End Basic Tabs   -->
</div>

@stop