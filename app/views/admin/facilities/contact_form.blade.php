@extends('layouts.backend')

@section('content')
<div class="alert-list-error"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    </div>
@endif
<div class="col-lg-12">
     <!--Basic Tabs   -->
    <div class="panel panel-default">
           <div class="panel-body">
                @include('admin.facilities.navigation',array('id'=>$facility->id,'tab'=>$tab))

               <div class="tab-content">

                {{ Form::model($facility, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.facilities.updateContact', $facility->id))) }}
                            <br/>
                            <div class="form-group">
                                {{ Form::label('tel', 'Tel:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('tel', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('mobile', 'Mobile:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('mobile', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('email', 'Email:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('email', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('contact_person', 'Contact Person:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('contact_person', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('contact_remark', 'Contact Remark:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('contact_remark', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('skype', 'Skype:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('skype', null, array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('contact_avaiable_time', 'Contact Time:', array('class'=>'col-md-2 control-label')) }}
                                <div class="col-sm-6">
                                    {{ Form::text('contact_avaiable_time', null, array('class'=>'form-control')) }}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">&nbsp;</label>
                                <div class="col-sm-6">
                                    {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
                                    {{ link_to_route('admin.facilities.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
                                </div>
                            </div>
                {{ Form::close() }}
                       
               </div>

           </div>
    </div>
    <!--End Basic Tabs   -->
</div>

@stop