@extends('layouts.backend')

@section('content')

<?php  $arr_active = BaseController::arrStatus(); ?>
<div class="col-lg-12">
	<div class="panel-body">
		<form role="form">
            <span class="">Category</span>
            {{ Form::select('fa', $categories, $fa) }}
			<span class="">Facility name</span>
            <input type="text" name="fana" value="{{$fana}}"></input>
            <button class="btn btn-primary" type="submit">Filter</button>
        </form>
		
	</div>
</div>

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>List Facilities
           <!--  <div style="float:right;">{{ link_to_route('admin.facilities.create', 'Add New Facility', null, array('class' => 'label label-success')) }}</div> -->
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($facilities->count() > 0)
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>ID</th>
				<th>Facility Name</th>
				<th>Category</th>
				<th>Priority</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($facilities as $facility)
				<tr>
					<td>{{ $facility->id }}</td>
					<td>{{ $facility->name }}</td>
                    <td>
                    	{{ Form::select('categories', $facility->getArrCategories(),null) }}
                    </td>
                    <td>{{ $facility->priority }}</td>
                    <td id="status_{{$facility->id}}"><span class="label label-success">{{ $arr_active[$facility->status] }}</span></td>
                    <td>
                    	{{ link_to_route('admin.facilities.edit', 'Edit', array($facility->id), array('class' => 'label label-success')) }}
                        <a class="label label-warning" href="{{route('admin.facilities.switchStatus',$facility->id)}}" remote="true">Enable/Disable</a>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.facilities.destroy', $facility->id))) }}
                            {{ Form::submit('Delete', array('class' => 'label-danger','data-confirm'=>'true')) }}
                        {{ Form::close() }}
                        
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no Facility
@endif
				</div>
			</div>
		</div>
		@include('shared.pagination',array('obs'=>$facilities))
	</div>
</div>
@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		$('a[remote="true"]').on("click", function(e) {
			e.preventDefault();
			var confir = confirm("Are you sure ?");
    		if (confir == true) {
        		var url = $(this).attr('href');
			$.ajax({
                    type: "post",
                    url : url,
                    data : "",
                    success : function(data){
                        console.log(data);
                        if(data['code'] == 'ok')
                        {
                        	if(data['message'] !== null)
                        	{
                        		var status_i = jQuery("#"+data['id']);
                        		jQuery(status_i).html(data['message']);
                        	}
                            alert('Success.');
                        }
                        else
                        {
                        	alert('Working had fail.');
                        }
                    }
                },"json");
    		} else {
        		return false;
    		}

			
		});
	</script>
@stop

