@extends('layouts.backend')

@section('content')
<div class="alert-list-error"></div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    </div>
@endif
<div class="col-lg-12">
     <!--Basic Tabs   -->
    <div class="panel panel-default">
           <div class="panel-body">
                @include('admin.facilities.navigation',array('id'=>$facility->id,'tab'=>$tab))
               <div class="tab-content">
                @if (!empty($categories))
                    
                    {{ Form::model($faciCategory, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.facilities.updateService', $facility->id))) }}
                     <br/> 
                    <div class="form-group">
                        {{ Form::label('category', 'Servie Category:', array('class'=>'col-md-2 control-label')) }}
                        <div class="col-sm-6">
                          {{ Form::select('category_id', $categories,null,array('class'=>'select input-fm','url'=>route('admin.facilities.getService',$facility->id))) }}
                        </div>
                    </div>
                    <div>
                        <table class="table table-striped table-bordered table-hover" id="plans_table">
                            <tr>
                                <th>Plan</th>
                                <th>Cost</th>
                                <th>Unit</th>
                                <th>Remark</th>
                                <th>
                                    <a class="btn btn-primary" data-toggle="modal" data-target="#myModal" href="#">Add New</a>
                                </th>
                            </tr>
                        @foreach ($plans as $plan)
                            <tr id="row_{{$plan->id}}">
                                <td>{{ $plan->plan_name }}</td>
                                <td>{{ $plan->plan_code }}</td>
                                <td>{{ $plan->plan_unit }}</td>
                                <td>{{ $plan->plan_description }}</td>
                                <td>
                                    <a href="{{route('admin.plans.updatePlan',array($facility->id,$plan->category_id,$plan->id))}}" plan-edit="true"> Edit</a>
                                    <a href="{{route('admin.plans.deletePlan',array($facility->id,$plan->category_id,$plan->id))}}" plan-delete="true"> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </table>
                    </div>

                    <div class="container"></div>

                    <div class="form-group">
                        {{ Form::label('basic_service_avaiable', 'Basic service avaiable:', array('class'=>'col-md-2 control-label')) }}
                        <div class="col-sm-6">
                            {{ Form::textarea('basic_service_avaiable', null, array('class'=>'form-control','rows'=>3)) }}
                        </div>
                    </div>
                
                    <div class="form-group">
                        {{ Form::label('basic_service_notavaiable', 'Basic service not avaiable:', array('class'=>'col-md-2 control-label')) }}
                        <div class="col-sm-6">
                            {{ Form::textarea('basic_service_notavaiable', null, array('class'=>'form-control','rows'=>3)) }}
                        </div>
                    </div>
                
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-6">
                             {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
                             {{ link_to_route('admin.facilities.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
                        </div>
                    </div>
                    {{ Form::close() }}
                @endif

               </div>

           </div>
    </div>
    <!--End Basic Tabs   -->
    <!--  Modals-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">New Plan</h4>
                </div>
                <div class="modal-body">
                    <form role="form">
                        <input type="hidden" value="{{$facility->id}}" name="id" id="dialog_facility_id"></input>
                        <input type="hidden" value="{{App::getLocale()}}" name="lang" id="dialog_facility_lang"></input>
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" name="plan_name">
                        </div>

                        <div class="form-group">
                            <label>Cost</label>
                            <input class="form-control" name="plan_cost">
                        </div>

                        <div class="form-group">
                            <label>Unit</label>
                            <input class="form-control" name="plan_unit">
                        </div>

                        <div class="form-group">
                            <label>Remark</label>
                            <textarea rows="3" class="form-control" name="plan_description"></textarea>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" id="add_new_plan">Save changes</button>
                        </div>
                    </form>
                </div>
                
            </div>
        </div>
    </div>

    <!-- End Modals-->
    <div class="form-edit-dialog"></div>
</div>

@stop

{{-- Scripts --}}
@section('scripts')
<script type="text/javascript">
     $('#add_new_plan').on("click", function(e) {
        e.preventDefault();
        var dataForm = $(this).closest("form").serialize();
        var idCategory = $('select[name=category_id]').val();
        var idFa       = $('#dialog_facility_id').val();
        var lang = $('#dialog_facility_lang').val();
        $.ajax({
             type: "post",
             url : '/'+lang+'/admin/facilities/'+idFa+'/cate/'+idCategory,
             data : dataForm,
             success : function(data){
                if (data['code'] == '1'){
                    $("div#myModal").modal('hide');
                    //$('#plans_table tr:last').after(data['message']);
                    window.location.reload(true);
                }
             }
          },"json");
     });
     //
     
     $('a[plan-delete=true]').on("click", function(e) {
        var answer = confirm('Are you sure you want to delete this?');
        if (answer)
        {
            e.preventDefault();
            var url = $(this).attr('href');
            var currentRow = $(this).closest('tr');
            $.ajax({
             type: "delete",
             url : url,
             data : '',
             success : function(data){
             }
            },"json");
            //
            currentRow.remove();
        }
        else
        {
          e.preventDefault();
          return false;
        }
      });

     //
     $('a[plan-edit=true]').on("click", function(e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.ajax({
             type: "get",
             url : url,
             data : '',
             success : function(dataEdit){
                //console.log(dataEdit['message']);
                $('div.form-edit-dialog').html(dataEdit['message']);
                $("div#myModalEdit").modal('show');
             }
            },"json");
            return false;
            //
      });
     
</script>

<script type="text/javascript">
    $( "select" ).change(function() {
    var url = $('select').attr('url');
    var category_id = $( this ).val();
    window.location.href = url+'?category_id='+category_id;
    });
</script>
@stop