@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>{{ trans('user.edit') }}
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::model($user, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.users.update', $user->id))) }}

        <div class="form-group">
            {{ Form::label('status', trans('user.status'), array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
                {{ Form::select('status', array(ACTIVE => trans('user.active'), INACTIVE => trans('user.inactive')),$user->status) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('description', trans('user.des'), array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-6">
      {{ Form::submit(trans('user.update'), array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.users.index', trans('user.cancel'), null, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop