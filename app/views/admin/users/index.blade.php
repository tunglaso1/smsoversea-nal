@extends('layouts.backend')

@section('content')

<?php  $arr_active = BaseController::arrStatus(); ?>

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>{{ trans('user.list_user') }}
            <div style="float:right;">{{ link_to_route('admin.users.create', trans('user.add_new_user'), null, array('class' => 'label label-success')) }}</div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($users->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>{{ trans('user.username') }}</th>
				<th>{{ trans('user.email') }}</th>
				<th>{{ trans('user.des') }}</th>
				<th>{{ trans('user.status') }}</th>
				<th>{{ trans('user.action') }}</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($users as $user)
				<tr>
					<td>{{ $user->username }}</td>
					<td>{{ $user->email }}</td>
					<td>{{ Str::limit($user->description, 40) }}</td>
					<td id="status_{{$user->id}}"><span class="label label-success">{{ $arr_active[$user->status] }}</span></td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.users.destroy', $user->id))) }}
                            {{ Form::submit(trans('user.delete'), array('class' => 'label-warning','data-confirm'=>'true')) }}
                        {{ Form::close() }}
                        {{ link_to_route('admin.users.edit', trans('user.edit'), array($user->id), array('class' => 'label label-success')) }}
                        {{ link_to_route('admin.users.switch', trans('user.switch'), array($user->id), array('class' => 'label label-success','remote'=>'true')) }}
                        {{ link_to_route('admin.users.resetPass', trans('user.reset_pass'), array($user->id), array('class' => 'label label-success','remote'=>'true')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no books
@endif
				</div>
			</div>
		</div>
		@include('shared.pagination',array('obs'=>$users))
	</div>
</div>


@stop

{{-- Scripts --}}
@section('scripts')
	<script type="text/javascript">
		$('a[remote="true"]').on("click", function(e) {
			e.preventDefault();
			var confir = confirm("Are you sure ?");
    		if (confir == true) {
        		var url = $(this).attr('href');
			$.ajax({
                    type: "get",
                    url : url,
                    data : "",
                    success : function(data){
                        //console.log(data);
                        if(data['code'] == 'ok')
                        {
                        	if(data['message'] !== null)
                        	{
                        		var status_i = jQuery("#"+data['id']);
                        		jQuery(status_i).html(data['message']);
                        	}
                            alert('Success.');
                        }
                        else
                        {
                        	alert('Working had fail.');
                        }
                    }
                },"json");
    		} else {
        		return false;
    		}

			
		});
	</script>
@stop
