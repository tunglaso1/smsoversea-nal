@extends('layouts.backend')

@section('content')

<?php  $arr_active = array(ACTIVE => trans('user.active'), INACTIVE => trans('user.inactive')); ?>

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i> {{ trans('user.show') }}
            <div style="float:right;">
            	{{ link_to_route('admin.users.index', trans('user.all_user'), null, array('class'=>'label label-success')) }}
        	</div>
        </div>
        <div class="panel-body">

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	<tr>
		<td>{{trans('user.username')}}</td>
		<td>{{ $user->username }}</td>
	</tr>

	<tr>
		<td>{{ trans('user.email')}}</td>
		<td>{{ $user->email }}</td>
	</tr>
	
	<tr>
		<td>{{trans('user.status')}}</td>
		<td><span class="label label-success">{{ $arr_active[$user->status] }}</span></td>
	</tr>

	<tr>
		<td>{{trans('user.des')}}</td>
		<td>{{{ $user->description }}}</td>
	</tr>

</table>
</div>
</div>
</div>

@stop
