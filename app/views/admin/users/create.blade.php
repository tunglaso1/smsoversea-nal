@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>{{ trans('user.create')}}
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::open(array('route' => 'admin.users.store', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('name', trans('user.username'), array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('username', Input::old('username'), array('class'=>'form-control', 'placeholder'=>'Username')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('email', trans('user.email'), array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'Email')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password', trans('user.password'), array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Password')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password_confirmation', trans('user.password_confirm'), array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::password('password_confirmation', array('class'=>'form-control', 'placeholder'=>'Password Confirmation')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('description', trans('user.des'), array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit(trans('user.create'), array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop


