@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Edit Filter
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::model($filter, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.filters.update', $filter->id))) }}

        <div class="form-group">
            {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ $filter->criteria}}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('des', 'Description:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ $filter->description}}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('status', 'Status:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::checkbox('status',1, $filter->status) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('type', 'Type:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::checkbox('type',1, $filter->type) }}
            </div>
        </div>


<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-6">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.filters.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop