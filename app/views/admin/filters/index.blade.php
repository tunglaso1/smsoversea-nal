@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>List Filters
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($filters->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>Name</th>
				<th>Des</th>
				<th>Status</th>
				<th>Type</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($filters as $filter)
				<tr>
					<td>{{ $filter->criteria }}</td>
					<td>{{ $filter->description }}</td>
					<td>{{ $filter->status == ACTIVE ? 'Enable' : 'Disable' }}</td>
					<td>{{ $filter->type == INACTIVE ? 'Select Box' : 'Multi Select' }}</td>
                    <td>
                    	{{ link_to_route('admin.filters.edit', 'Edit', array($filter->id), array('class' => 'label label-success')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no filter
@endif
				</div>
			</div>
		</div>
	</div>
</div>


@stop
