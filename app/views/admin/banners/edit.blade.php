@extends('layouts.backend')

@section('content')

<?php  $arr_actives = BaseController::arrStatus(); ?>


<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Edit Banner
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::model($banner, array('class' => 'form-horizontal', 'method' => 'PATCH','files'=>true, 'route' => array('admin.banners.update', $banner->id))) }}

        <div class="form-group">
            {{ Form::label('banner_position', 'Banner Type:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::select('banner_position', array(BANNER_HEADER=>'Header',BANNER_RIGHT=>'Right'),$banner->banner_position,array('class'=>'col-md-3')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('banner_status', 'Banner Status:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::select('banner_status', $arr_actives,$banner->banner_status,array('class'=>'col-md-3')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('priority', 'Priority:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('priority', Input::old('priority'), array('class'=>'col-md-3')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('start', 'Start:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('start', Input::old('banner_mobile'), array('class'=>'datetime_picker')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('end', 'End:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('end', Input::old('end'), array('class'=>'datetime_picker')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('image', 'Image:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-9">
              {{ Form::file('image', array('class'=>'col-md-4')) }}
              {{ HTML::image($banner->getThumb(), $alt="") }}
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-6">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.banners.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop