@extends('layouts.backend')

@section('content')

<?php  $arr_actives = BaseController::arrStatus(); ?>

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>List banners
            <div style="float:right;">{{ link_to_route('admin.banners.create', 'Add New banner', null, array('class' => 'label label-success')) }}</div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($banners->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>Id</th>
				<th>Position</th>
				<th>Priority</th>
				<th>Status</th>
				<th>Start</th>
				<th>End</th>
				<th>Image</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($banners as $banner)
				<tr>
					<td>{{ $banner->id }}</td>
					<td>{{ $banner->banner_position }}</td>
					<td>{{ $banner->priority }}</td>
					<td>{{ $arr_actives[$banner->banner_status] }}</td>
					<td>{{ $banner->start }}</td>
					<td>{{ $banner->end }}</td>
					<td>
						{{ HTML::image($banner->getThumb(), $alt="thumbnail") }}
					</td>
                    <td>
                    	{{ link_to_route('admin.banners.edit', 'Edit', array($banner->id), array('class' => 'label label-success')) }}
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.banners.destroy', $banner->id))) }}
                            {{ Form::submit('Delete', array('class' => 'label-warning','data-confirm'=>'true')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no banner
@endif
				</div>
			</div>
		</div>
		@include('shared.pagination',array('obs'=>$banners))
	</div>
</div>


@stop
