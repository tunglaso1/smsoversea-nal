@extends('layouts.backend')

@section('content')

<?php  $arr_active = array(ACTIVE => 'Active', INACTIVE => 'Inactive'); ?>

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i> User Detail
            <div style="float:right;">
            	{{ link_to_route('admin.pages.index', 'All pages', null, array('class'=>'label label-success')) }}
        	</div>
        </div>
    </div>
</div>
<div class="col-lg-12">
	{{ $page->content }}
</div>

@stop
