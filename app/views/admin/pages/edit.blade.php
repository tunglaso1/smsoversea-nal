@extends('layouts.backend')
@section('scriptsck')
    <script src="/assets/ckeditor/ckeditor.js"></script>
@stop

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Edit Page
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::model($page, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.pages.update', $page->id))) }}

        <div class="form-group">
            {{ Form::label('name', 'Name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
                 {{ Form::select('name', array(SP_SEVICE_INFO=>SP_SEVICE_INFO,SP_TERM_SERVICE=>SP_TERM_SERVICE,SP_AD=>SP_AD,SP_ABOUT_US=>SP_ABOUT_US),$page->name) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('status', trans('user.status'), array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
                {{ Form::select('status', array(ACTIVE => trans('user.active'), INACTIVE => trans('user.inactive')),$page->status) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-12">
              {{ Form::textarea('content', $page->content, array('id'=>'editor1')) }}
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-6">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.pages.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop

{{-- Scripts --}}
@section('scripts')
    <script type="text/javascript">
        // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace( 'editor1',
                {
                filebrowserBrowseUrl : '/assets/ckeditor/ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : '/assets/ckeditor/ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : '/assets/ckeditor/ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : '/assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : '/assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : '/assets/ckeditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                } 
                );
    </script>
@stop