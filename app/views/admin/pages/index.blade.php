@extends('layouts.backend')

@section('content')

<?php  $arr_active = BaseController::arrStatus(); ?>

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>List Pages
            <div style="float:right;">{{ link_to_route('admin.pages.create', 'Add New Pages', null, array('class' => 'label label-success')) }}</div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($pages->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>ID</th>
				<th>NAME</th>
				<th>STATUS</th>
				<th>Created User</th>
				<th>Updated User</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($pages as $page)
				<tr>
					<td>{{ $page->id }}</td>
					<td>{{ $page->name }}</td>
					<td>{{ $arr_active[$page->status] }}</td>
                    <td>{{ $page->create_user }}</td>
                    <td>{{ $page->update_user }}</td>
                    <td>
                        {{ link_to_route('admin.pages.show', 'Show', array($page->id), array('class' => 'label label-success')) }}
                        {{ link_to_route('admin.pages.edit', 'Edit', array($page->id), array('class' => 'label label-success')) }}
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.pages.destroy', $page->id))) }}
                            {{ Form::submit('Delete', array('class' => 'label-warning','data-confirm'=>'true')) }}
                        {{ Form::close() }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no categories
@endif
				</div>
			</div>
		</div>
	</div>
</div>


@stop
