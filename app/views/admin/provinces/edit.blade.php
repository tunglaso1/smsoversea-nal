@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Edit Province
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::model($province, array('class' => 'form-horizontal', 'method' => 'PATCH', 'route' => array('admin.provinces.update', $province->id))) }}
        @foreach ($province->languagecontents as $lang)
        <div class="form-group">
            {{ Form::label($lang->language_code.'[name]', $lang->language_code.' Name', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text($lang->language_code.'[name]', $lang->content, array('class'=>'form-control')) }}
            </div>
        </div>
        @endforeach

        <div class="form-group">
            {{ Form::label('is_view_district', 'Is View District:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::checkbox('major[is_view_district]',1,$province->is_view_district) }}
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-6">
      {{ Form::submit('Update', array('class' => 'btn btn-lg btn-primary')) }}
      {{ link_to_route('admin.provinces.index', 'Cancel', null, array('class' => 'btn btn-lg btn-default')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop