@extends('layouts.backend')

@section('content')

<?php  $arr_active = array(ENABLE_DISTRICT => 'Yes', DISENABLE_DISTRICT => 'No'); ?>

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Adminnistrative Divisions
            <div style="float:right;">{{ link_to_route('admin.provinces.create', 'Add New Province', null, array('class' => 'label label-success')) }}</div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($provinces->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>Name Eng</th>
				<th>Name Thai</th>
				<th>Is View District</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($provinces as $province)
				<tr>
					@foreach ($province->languagecontents as $name)
				  		<td>
				  			@unless (is_null($name))
				  				{{ $name->content }}
				  			@endunless
				  		</td>
					@endforeach
					<td><span class="label label-success">{{ $province->is_view_district? 'True' : 'False' }}</span></td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.provinces.destroy', $province->id))) }}
                            {{ Form::submit('Delete', array('class' => 'label-warning','data-confirm'=>'true')) }}
                        {{ Form::close() }}
                        {{ link_to_route('admin.provinces.edit', 'Edit', array($province->id), array('class' => 'label label-success')) }}
                        {{ link_to_route('admin.provinces.districts.index','Districts',$province->id,array('class' => 'label label-success')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no provinces
@endif
				</div>
			</div>
		</div>
		
	</div>
</div>


@stop
