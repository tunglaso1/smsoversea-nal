@extends('layouts.backend')

@section('content')

<?php  $arr_active = BaseController::arrStatus(); ?>

<div class="col-lg-12">
    <!-- Advanced Tables -->
    <div class="panel panel-primary">
    	<div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>List Categories
            <div style="float:right;">{{ link_to_route('admin.categories.create', 'Add New Category', null, array('class' => 'label label-success')) }}</div>
        </div>
        <div class="panel-body">
            <div class="table-responsive">
	@if ($categories->count())
	<table class="table table-striped table-bordered table-hover" id="dataTables-example">
		<thead>
			<tr>
				<th>Category Name</th>
				<th>Description</th>
				<th>Created User</th>
				<th>Updated User</th>
				<th>Action</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($categories as $category)
				<tr>
					<td>{{ $category->name }}</td>
					<td>{{ Str::limit($category->description, 40) }}</td>
                    <td>{{ $category->create_user }}</td>
                    <td>{{ $category->update_user }}</td>
                    <td>
                        {{ Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'route' => array('admin.categories.destroy', $category->id))) }}
                            {{ Form::submit('Delete', array('class' => 'label-warning','data-confirm'=>'true')) }}
                        {{ Form::close() }}
                        {{ link_to_route('admin.categories.edit', 'Edit', array($category->id), array('class' => 'label label-success')) }}
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
@else
	There are no categories
@endif
				</div>
			</div>
		</div>
		@include('shared.pagination',array('obs'=>$categories))
	</div>
</div>


@stop
