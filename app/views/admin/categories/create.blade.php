@extends('layouts.backend')

@section('content')

<div class="col-lg-12">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <i class="fa fa-clock-o fa-fw"></i>Create User
        </div>
        <div class="panel-body">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
                </ul>
            </div>
            @endif

{{ Form::open(array('route' => 'admin.categories.store', 'class' => 'form-horizontal')) }}

        <div class="form-group">
            {{ Form::label('name', 'Category name:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::text('name', Input::old('name'), array('class'=>'form-control', 'placeholder'=>'Category name')) }}
            </div>
        </div>

<!--        <div class="form-group">-->
<!--            {{ Form::label('email', 'Email:', array('class'=>'col-md-2 control-label')) }}-->
<!--            <div class="col-sm-6">-->
<!--              {{ Form::text('email', Input::old('email'), array('class'=>'form-control', 'placeholder'=>'Email')) }}-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="form-group">-->
<!--            {{ Form::label('password', 'Password:', array('class'=>'col-md-2 control-label')) }}-->
<!--            <div class="col-sm-6">-->
<!--              {{ Form::password('password', Input::old('password'), array('class'=>'col-sm-6', 'placeholder'=>'Password')) }}-->
<!--            </div>-->
<!--        </div>-->
<!---->
<!--        <div class="form-group">-->
<!--            {{ Form::label('password_confirmation', 'Confirmation:', array('class'=>'col-md-2 control-label')) }}-->
<!--            <div class="col-sm-6">-->
<!--              {{ Form::password('password_confirmation', Input::old('password_confirmation'), array('class'=>'form-control', 'placeholder'=>'Password Confirmation')) }}-->
<!--            </div>-->
<!--        </div>-->

        <div class="form-group">
            {{ Form::label('description', 'Description:', array('class'=>'col-md-2 control-label')) }}
            <div class="col-sm-6">
              {{ Form::textarea('description', Input::old('description'), array('class'=>'form-control', 'placeholder'=>'Description')) }}
            </div>
        </div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary')) }}
    </div>
</div>

{{ Form::close() }}
</div>
</div>
</div>

@stop


